import jp.lang.RT

RT.eval("""
declare fib;
def fib = fn(n) "fib-func" do(
    //println(n);
    if(n==0||n==1,1,fib(n-1)+fib(n-2));
);
//fib(2);
println(map(range(1,10),fib)); 
//println([fib(0),fib(1),fib(2),fib(3),fib(4)]); 
""")