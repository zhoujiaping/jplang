import jp.lang.RT

assert RT.eval("""
def x = 3;
loop(a,x)
    if(a>1,
        recur(a-1),
        a
    );
""").toString() == "1"
