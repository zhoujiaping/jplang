import jp.lang.RT

assert RT.eval("""
1 |> _ +1 |> println(_)  ;
[1,2,3] |> map(_,fn(x)x**2) |> println(_);
println(map([1,2,3],fn(x)x**2));
""")
