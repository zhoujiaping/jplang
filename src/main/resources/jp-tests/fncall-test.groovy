import jp.lang.RT

assert RT.eval("""
def f1 = fn()2;
def f2 = fn(x, y)x+y;
[f1(),f2(3,4), f2(...[5,6])];
""").toString() == "Vector(2, 7, 11)"