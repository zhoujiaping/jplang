import jp.lang.RT

assert RT.eval("""
def nick = "NICK";
def a = {
  name:"zhou",
  "age":18,
  (nick):"xxx"
};
""").toString() == "HashMap((NICK, xxx), (name, zhou), (age, 18))"
