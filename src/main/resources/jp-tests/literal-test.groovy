import jp.lang.RT

assert RT.eval("""
[1,"2",'\\t',
    {name:"jack",
     "age":18,
     },
     true,
     false,
     null,];
""").toString() == "Vector(1, 2, \t, HashMap((name, jack), (age, 18)), true, false, null)"
