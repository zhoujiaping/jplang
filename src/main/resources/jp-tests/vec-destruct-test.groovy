import jp.lang.RT

assert RT.eval("""
def o = ["a","b",[["c","d"],"e"],"f"];
def [A,B,...R as arr] = o;
[A,B,R,arr];
""").toString() == "Vector(a, b, Vector(Vector(Vector(c, d), e), f), Vector(a, b, Vector(Vector(c, d), e), f))"
