import jp.lang.RT

assert RT.eval("""
def a = 1;
def b = 2, c =3;
[a,b,c];
""").toString() == "Vector(1, 2, 3)"