import jp.lang.RT

RT.eval("""
def user = {
   name: "jack",
   age: 18,
   hobby:["swim","jogging"]  
};

def {
    name: name1,
    age: age1,
    hobby : [h1,h2] 
} = user;

println(name1);
println(age1);
println(h1);
println(h2);
""")
//jack
//18
//swim
//jogging
