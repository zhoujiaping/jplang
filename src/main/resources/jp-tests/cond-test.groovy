import jp.lang.RT

assert RT.eval("""
def x = 3;
cond(
    x<1 , "<1",
    x<2 , "<2",
    1   , ">=2"
);
""").toString() == ">=2"
