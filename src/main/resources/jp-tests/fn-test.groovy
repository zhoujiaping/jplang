import jp.lang.RT

assert RT.eval("""
def f1 = fn()2**3;
f1();
""").toString() == "8"

assert RT.eval("""
def f1 = fn(a,b,[c,{d:d}]) do(
    a+b+c+d;
);
f1(1,2,[3,{d:4}]);
""").toString() == "10"

assert RT.eval("""
def f1 = fn(a,b=2,[c,{d:d}])( a+b+c+d );
f1(1,null,[3,{d:4}]);
""").toString() == "10"