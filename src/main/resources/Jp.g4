grammar Jp;

/*
文件由若干个语句组成。
语句由 表达式后面跟一个分号组成。
表达式有 声明表达式、括号表达式、函数定义表达式、函数调用表达式、
        try表达式、throw表达式、if表达式、cond表达式、loop表达式、
        recur表达式、map表达式、vec表达式、字面量、属性访问表达式（a.b或者a["b"]），
        标识符、块表达式等。
函数调用表达式有前缀、中缀、括号三种方式。
前缀、中缀方式只支持固定的一些操作符。

字面量支持 字符、字符串、字节、整数、大整数、浮点数、大浮点数、科学计数法、分数、2到36任意进制（大）整数、vector、map。

字符串支持多行，模板字符串。

函数形参、变量声明等支持剩余参数收集、参数解构、默认参数。


antler4:
每个rule会生成语法树的一个节点，每个终结符对应叶子节点。
如果使用listener，每个rule会有对应的enter/exit。
如果rule的各个备选分支指定了 方法？（如DefExp），则会生成该方法的enter/exit。
rule的分支，要么全部指定 方法，要么全部不指定 方法。
如果为分支指定了 方法，则该rule本身不会生成enter/exit。
*/
file: stmt*;
stmt: expr ';';

expr:
//|defnExpr
'declare' NAME (',' NAME)* # Decl            //declare a
| 'def' struct '=' expr (',' struct '=' expr)* # Def
| expr '(' (EXTEND expr)? ')' # FnCall0  //str()   str(...names)
| expr '(' expr (',' expr)* (',' EXTEND expr)?')' # FnCallN// antler4 可以处理直接左递归，但是不能处理间接左递归。所以fnCall直接写在这里。
| 'try' expr ('catch' NAME expr)? ('finally' expr)? # Try
| 'throw' expr # Throw
| 'if' '(' expr ',' expr (',' expr)? ')' # If
| 'cond' '(' (expr ',' expr) (',' expr ',' expr)* ')' # Cond
| 'loop' '(' (structDefault ',' expr (',' structDefault ',' expr)*)? ')' expr # Loop
| 'recur' '(' ')' # Recur0
| 'recur' '(' expr (',' expr)* ')' # RecurN
| '{' '}' # emptyMap
| '{' kvPairs '}' # Map
| '[' ']' # emptyVec
| '[' expr(',' expr)* ','? ']' # Vec
| TMPL_STRING # tmplStr
| '(' expr ')' # Bracket
| expr '.' NAME # VisitAttr
| expr '[' expr ']' # VisitExprAttr
//<assoc=right>右结合
| <assoc=right> op=(NOT|BIT_NEG|SUB|ADD) expr # Pre
// 位置越靠前，优先级越高
| expr POW expr # Pow
| expr op=(MUL|DIV|REM/*|QUOT*/) expr # AritBin
| expr op=(ADD|SUB) expr # Sign
| expr op=(SHIFT_LEFT|SHIFT_RIGHT|SHIFT_RIGHT_UNSIGNED) expr # Shift
| expr op=(LT|LT_EQ|GT|GT_EQ) expr # Comp
| expr op=(EQ|NOT_EQ) expr # Equal
| expr op=(BIT_AND|BIT_XOR|BIT_OR) expr # BitLogic
| expr op=(AND|OR) expr # Logic
| expr op=(RANGE|RANGE_TO) expr # Range
//| BYTE # byte
| CHAR # char
| str # String
| '0' # zero //0
| INT10 # int10
| INT16 # int16
| INT_R # intR
// | RATIO # ratio 除法自动返回分数
| FLOAT # float
//|REG
| 'true' # true
| 'false' # false
| 'null' # null
| '_' # underline
| NAME # nameRef
| 'do' '(' stmt* ')' # Block
| 'fn(' (EXTEND struct)? ')' str? expr # Fn0 // fn()println("hello world!")   fn(...args)println(str(...args))
| 'fn(' structDefault? (','structDefault?)* (',' EXTEND struct)? ')' str? expr # FnN  // fn(x,y)x+y   fn(x,y,...rest)println(str(x,y,...rest))
| expr '|>' expr # pip
//| expr SUF
;

struct:
NAME # nameStruct
|'[' (EXTEND struct)? as? ']' # VecStruct0
|'[' structDefault (','structDefault)* (','|(',' EXTEND struct))? as? ']' # VecStructN
|'{' (EXTEND struct)? as? '}' # MapStruct0
|'{' structPair (',' structPair)* (','|(','  EXTEND struct))? as? '}' # MapStructN
;

structPair: NAME (':' structDefault)? ;
structDefault:struct ('=' expr)?;

as : 'as' NAME;

str: STRING;
// for Map
kvPairs: kvPair (',' kvPair)* (',')?;
kvPair:
 NAME ':' expr # NamePair
|str ':' expr # StrPair
|'(' expr '):' expr # ExprPair
;

CHAR: '\'' (('\\' (['\\/bfnrt]|UNICODE))|~['\\]) '\'';
//BYTE: '0'[bB]HEX+; //???
//| [\-+]? '0'[0-7]+ 'N'?  //8进制
INT10: [1-9][0-9]* 'N'?;  //10进制
INT16: '0'[xX][0-9A-Fa-f]+ 'N'?; //16进制
INT_R: [1-9][0-9]?[rR][0-9A-Za-z]+ 'N'? ;//2-36任意进制

//FLOAT: [\-+]?[0-9]+('.'[0-9]*)([eE][\-+]?[0-9]+)?'M'?;
FLOAT: [0-9]+('.'[0-9]*)([eE][\-+]?[0-9]+)?'M'?;
//RATIO: [\-+]?[0-9]+'/'[0-9]+'F';
//POS:'+';
//NEG:'-';

NOT:'!';
BIT_NEG:'~';
EXTEND: '...';

POW:'**';

MUL:'*';
DIV:'/';
REM:'%';
//QUOT:'//';

ADD:'+';
SUB:'-';

SHIFT_LEFT:'<<';
SHIFT_RIGHT:'>>';
SHIFT_RIGHT_UNSIGNED:'>>>';

LT:'<';
LT_EQ:'<=';
GT:'>';
GT_EQ:'>=';

EQ:'==';
NOT_EQ: '!=';

BIT_AND:'&';
BIT_XOR:'^';
BIT_OR:'|';
AND:'&&';
OR:'||';

RANGE:'..<';
RANGE_TO:'..';

// ASSIGN:'=';

/*
优先级
**
*,/,//,%
+,-,
>,<,>=,<=,==,!=
&&,||,
*/



NAME : [_$a-zA-Z][_$a-zA-Z0-9]*;//TODO 字符集

//REG:
// '/' '/' 'i'?'g'?'m'?
//|'$/' '/' 'i'?'g'?'m'?;

STRING: '"' (ESC | ~["\\])* '"';

TMPL_STRING: '"""' (ESC | ~["\\])* '"""';
//INT: [\-+]?'0'  //0
//| [\-+]? [1-9][0-9]* 'N'?  //10进制
//| [\-+]? '0'[xX][0-9A-Fa-f]+ 'N'? //16进制
////| [\-+]? '0'[0-7]+ 'N'?  //8进制
//| [\-+]? [1-9][0-9]?[rR][0-9A-Za-z]+ 'N'? //2-36任意进制
//;



WS: [ \t\n\r]+ -> skip ;
LINE_COMMENT: '//'.*? '\n' -> skip;
COMMENT: '/*'.*? '*/' -> skip;

fragment ESC : '\\' (["\\/bfnrt]|UNICODE);
fragment UNICODE: 'u' HEX HEX HEX HEX;
fragment HEX: [0-9A-Fa-f];
//    static Pattern intPat = Pattern.compile("([-+]?)(?:(0)|([1-9][0-9]*)|0[xX]([0-9A-Fa-f]+)|0([0-7]+)|([1-9][0-9]?)[rR]([0-9A-Za-z]+)|0[0-9]+)(N)?");
//    static Pattern ratioPat = Pattern.compile("([-+]?[0-9]+)/([0-9]+)");
//    static Pattern floatPat = Pattern.compile("([-+]?[0-9]+(\\.[0-9]*)?([eE][-+]?[0-9]+)?)(M)?");







