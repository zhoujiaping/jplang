// Generated from E:/gitlab-repo/jplang/src/main/resources/Jp.g4 by ANTLR 4.13.1
package jp;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link JpParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface JpVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link JpParser#file}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFile(JpParser.FileContext ctx);
	/**
	 * Visit a parse tree produced by {@link JpParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt(JpParser.StmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Pre}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPre(JpParser.PreContext ctx);
	/**
	 * Visit a parse tree produced by the {@code underline}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnderline(JpParser.UnderlineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code VisitAttr}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVisitAttr(JpParser.VisitAttrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Fn0}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFn0(JpParser.Fn0Context ctx);
	/**
	 * Visit a parse tree produced by the {@code RecurN}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecurN(JpParser.RecurNContext ctx);
	/**
	 * Visit a parse tree produced by the {@code String}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(JpParser.StringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code float}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloat(JpParser.FloatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FnCall0}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFnCall0(JpParser.FnCall0Context ctx);
	/**
	 * Visit a parse tree produced by the {@code Comp}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComp(JpParser.CompContext ctx);
	/**
	 * Visit a parse tree produced by the {@code emptyVec}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptyVec(JpParser.EmptyVecContext ctx);
	/**
	 * Visit a parse tree produced by the {@code zero}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitZero(JpParser.ZeroContext ctx);
	/**
	 * Visit a parse tree produced by the {@code AritBin}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAritBin(JpParser.AritBinContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tmplStr}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplStr(JpParser.TmplStrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Equal}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqual(JpParser.EqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Throw}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThrow(JpParser.ThrowContext ctx);
	/**
	 * Visit a parse tree produced by the {@code pip}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPip(JpParser.PipContext ctx);
	/**
	 * Visit a parse tree produced by the {@code emptyMap}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptyMap(JpParser.EmptyMapContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Try}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTry(JpParser.TryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Logic}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogic(JpParser.LogicContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Loop}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop(JpParser.LoopContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Shift}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShift(JpParser.ShiftContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitLogic}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitLogic(JpParser.BitLogicContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Bracket}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBracket(JpParser.BracketContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Cond}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCond(JpParser.CondContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Def}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDef(JpParser.DefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FnN}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFnN(JpParser.FnNContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Recur0}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecur0(JpParser.Recur0Context ctx);
	/**
	 * Visit a parse tree produced by the {@code false}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFalse(JpParser.FalseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Decl}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecl(JpParser.DeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nameRef}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNameRef(JpParser.NameRefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Range}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRange(JpParser.RangeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code int16}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInt16(JpParser.Int16Context ctx);
	/**
	 * Visit a parse tree produced by the {@code FnCallN}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFnCallN(JpParser.FnCallNContext ctx);
	/**
	 * Visit a parse tree produced by the {@code VisitExprAttr}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVisitExprAttr(JpParser.VisitExprAttrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intR}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntR(JpParser.IntRContext ctx);
	/**
	 * Visit a parse tree produced by the {@code null}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNull(JpParser.NullContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Vec}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVec(JpParser.VecContext ctx);
	/**
	 * Visit a parse tree produced by the {@code int10}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInt10(JpParser.Int10Context ctx);
	/**
	 * Visit a parse tree produced by the {@code char}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChar(JpParser.CharContext ctx);
	/**
	 * Visit a parse tree produced by the {@code true}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrue(JpParser.TrueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Block}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(JpParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Pow}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPow(JpParser.PowContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Sign}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSign(JpParser.SignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code If}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf(JpParser.IfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Map}
	 * labeled alternative in {@link JpParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMap(JpParser.MapContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nameStruct}
	 * labeled alternative in {@link JpParser#struct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNameStruct(JpParser.NameStructContext ctx);
	/**
	 * Visit a parse tree produced by the {@code VecStruct0}
	 * labeled alternative in {@link JpParser#struct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVecStruct0(JpParser.VecStruct0Context ctx);
	/**
	 * Visit a parse tree produced by the {@code VecStructN}
	 * labeled alternative in {@link JpParser#struct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVecStructN(JpParser.VecStructNContext ctx);
	/**
	 * Visit a parse tree produced by the {@code MapStruct0}
	 * labeled alternative in {@link JpParser#struct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMapStruct0(JpParser.MapStruct0Context ctx);
	/**
	 * Visit a parse tree produced by the {@code MapStructN}
	 * labeled alternative in {@link JpParser#struct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMapStructN(JpParser.MapStructNContext ctx);
	/**
	 * Visit a parse tree produced by {@link JpParser#structPair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStructPair(JpParser.StructPairContext ctx);
	/**
	 * Visit a parse tree produced by {@link JpParser#structDefault}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStructDefault(JpParser.StructDefaultContext ctx);
	/**
	 * Visit a parse tree produced by {@link JpParser#as}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAs(JpParser.AsContext ctx);
	/**
	 * Visit a parse tree produced by {@link JpParser#str}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStr(JpParser.StrContext ctx);
	/**
	 * Visit a parse tree produced by {@link JpParser#kvPairs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKvPairs(JpParser.KvPairsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NamePair}
	 * labeled alternative in {@link JpParser#kvPair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNamePair(JpParser.NamePairContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StrPair}
	 * labeled alternative in {@link JpParser#kvPair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrPair(JpParser.StrPairContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprPair}
	 * labeled alternative in {@link JpParser#kvPair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprPair(JpParser.ExprPairContext ctx);
}