// Generated from E:/gitlab-repo/jplang/src/main/resources/Jp.g4 by ANTLR 4.13.1
package jp;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue"})
public class JpParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.13.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		CHAR=32, INT10=33, INT16=34, INT_R=35, FLOAT=36, NOT=37, BIT_NEG=38, EXTEND=39, 
		POW=40, MUL=41, DIV=42, REM=43, ADD=44, SUB=45, SHIFT_LEFT=46, SHIFT_RIGHT=47, 
		SHIFT_RIGHT_UNSIGNED=48, LT=49, LT_EQ=50, GT=51, GT_EQ=52, EQ=53, NOT_EQ=54, 
		BIT_AND=55, BIT_XOR=56, BIT_OR=57, AND=58, OR=59, RANGE=60, RANGE_TO=61, 
		NAME=62, STRING=63, TMPL_STRING=64, WS=65, LINE_COMMENT=66, COMMENT=67;
	public static final int
		RULE_file = 0, RULE_stmt = 1, RULE_expr = 2, RULE_struct = 3, RULE_structPair = 4, 
		RULE_structDefault = 5, RULE_as = 6, RULE_str = 7, RULE_kvPairs = 8, RULE_kvPair = 9;
	private static String[] makeRuleNames() {
		return new String[] {
			"file", "stmt", "expr", "struct", "structPair", "structDefault", "as", 
			"str", "kvPairs", "kvPair"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "';'", "'declare'", "','", "'def'", "'='", "'('", "')'", "'try'", 
			"'catch'", "'finally'", "'throw'", "'if'", "'cond'", "'loop'", "'recur'", 
			"'{'", "'}'", "'['", "']'", "'.'", "'0'", "'true'", "'false'", "'null'", 
			"'_'", "'do'", "'fn('", "'|>'", "':'", "'as'", "'):'", null, null, null, 
			null, null, "'!'", "'~'", "'...'", "'**'", "'*'", "'/'", "'%'", "'+'", 
			"'-'", "'<<'", "'>>'", "'>>>'", "'<'", "'<='", "'>'", "'>='", "'=='", 
			"'!='", "'&'", "'^'", "'|'", "'&&'", "'||'", "'..<'", "'..'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, "CHAR", "INT10", "INT16", 
			"INT_R", "FLOAT", "NOT", "BIT_NEG", "EXTEND", "POW", "MUL", "DIV", "REM", 
			"ADD", "SUB", "SHIFT_LEFT", "SHIFT_RIGHT", "SHIFT_RIGHT_UNSIGNED", "LT", 
			"LT_EQ", "GT", "GT_EQ", "EQ", "NOT_EQ", "BIT_AND", "BIT_XOR", "BIT_OR", 
			"AND", "OR", "RANGE", "RANGE_TO", "NAME", "STRING", "TMPL_STRING", "WS", 
			"LINE_COMMENT", "COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Jp.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public JpParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FileContext extends ParserRuleContext {
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public FileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_file; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitFile(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FileContext file() throws RecognitionException {
		FileContext _localctx = new FileContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_file);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(23);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 2)) & ~0x3f) == 0 && ((1L << (_la - 2)) & 8070463862819356245L) != 0)) {
				{
				{
				setState(20);
				stmt();
				}
				}
				setState(25);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StmtContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitStmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StmtContext stmt() throws RecognitionException {
		StmtContext _localctx = new StmtContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26);
			expr(0);
			setState(27);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class PreContext extends ExprContext {
		public Token op;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode NOT() { return getToken(JpParser.NOT, 0); }
		public TerminalNode BIT_NEG() { return getToken(JpParser.BIT_NEG, 0); }
		public TerminalNode SUB() { return getToken(JpParser.SUB, 0); }
		public TerminalNode ADD() { return getToken(JpParser.ADD, 0); }
		public PreContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitPre(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class UnderlineContext extends ExprContext {
		public UnderlineContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitUnderline(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class VisitAttrContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode NAME() { return getToken(JpParser.NAME, 0); }
		public VisitAttrContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitVisitAttr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Fn0Context extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode EXTEND() { return getToken(JpParser.EXTEND, 0); }
		public StructContext struct() {
			return getRuleContext(StructContext.class,0);
		}
		public StrContext str() {
			return getRuleContext(StrContext.class,0);
		}
		public Fn0Context(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitFn0(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class RecurNContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public RecurNContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitRecurN(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class StringContext extends ExprContext {
		public StrContext str() {
			return getRuleContext(StrContext.class,0);
		}
		public StringContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitString(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FloatContext extends ExprContext {
		public TerminalNode FLOAT() { return getToken(JpParser.FLOAT, 0); }
		public FloatContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitFloat(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FnCall0Context extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode EXTEND() { return getToken(JpParser.EXTEND, 0); }
		public FnCall0Context(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitFnCall0(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class CompContext extends ExprContext {
		public Token op;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode LT() { return getToken(JpParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(JpParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(JpParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(JpParser.GT_EQ, 0); }
		public CompContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitComp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class EmptyVecContext extends ExprContext {
		public EmptyVecContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitEmptyVec(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ZeroContext extends ExprContext {
		public ZeroContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitZero(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class AritBinContext extends ExprContext {
		public Token op;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode MUL() { return getToken(JpParser.MUL, 0); }
		public TerminalNode DIV() { return getToken(JpParser.DIV, 0); }
		public TerminalNode REM() { return getToken(JpParser.REM, 0); }
		public AritBinContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitAritBin(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class TmplStrContext extends ExprContext {
		public TerminalNode TMPL_STRING() { return getToken(JpParser.TMPL_STRING, 0); }
		public TmplStrContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitTmplStr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class EqualContext extends ExprContext {
		public Token op;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode EQ() { return getToken(JpParser.EQ, 0); }
		public TerminalNode NOT_EQ() { return getToken(JpParser.NOT_EQ, 0); }
		public EqualContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ThrowContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ThrowContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitThrow(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class PipContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public PipContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitPip(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class EmptyMapContext extends ExprContext {
		public EmptyMapContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitEmptyMap(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class TryContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode NAME() { return getToken(JpParser.NAME, 0); }
		public TryContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitTry(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class LogicContext extends ExprContext {
		public Token op;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode AND() { return getToken(JpParser.AND, 0); }
		public TerminalNode OR() { return getToken(JpParser.OR, 0); }
		public LogicContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitLogic(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class LoopContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<StructDefaultContext> structDefault() {
			return getRuleContexts(StructDefaultContext.class);
		}
		public StructDefaultContext structDefault(int i) {
			return getRuleContext(StructDefaultContext.class,i);
		}
		public LoopContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitLoop(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ShiftContext extends ExprContext {
		public Token op;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode SHIFT_LEFT() { return getToken(JpParser.SHIFT_LEFT, 0); }
		public TerminalNode SHIFT_RIGHT() { return getToken(JpParser.SHIFT_RIGHT, 0); }
		public TerminalNode SHIFT_RIGHT_UNSIGNED() { return getToken(JpParser.SHIFT_RIGHT_UNSIGNED, 0); }
		public ShiftContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitShift(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class BitLogicContext extends ExprContext {
		public Token op;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode BIT_AND() { return getToken(JpParser.BIT_AND, 0); }
		public TerminalNode BIT_XOR() { return getToken(JpParser.BIT_XOR, 0); }
		public TerminalNode BIT_OR() { return getToken(JpParser.BIT_OR, 0); }
		public BitLogicContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitBitLogic(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class BracketContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public BracketContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitBracket(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class CondContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public CondContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitCond(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class DefContext extends ExprContext {
		public List<StructContext> struct() {
			return getRuleContexts(StructContext.class);
		}
		public StructContext struct(int i) {
			return getRuleContext(StructContext.class,i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public DefContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitDef(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FnNContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<StructDefaultContext> structDefault() {
			return getRuleContexts(StructDefaultContext.class);
		}
		public StructDefaultContext structDefault(int i) {
			return getRuleContext(StructDefaultContext.class,i);
		}
		public TerminalNode EXTEND() { return getToken(JpParser.EXTEND, 0); }
		public StructContext struct() {
			return getRuleContext(StructContext.class,0);
		}
		public StrContext str() {
			return getRuleContext(StrContext.class,0);
		}
		public FnNContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitFnN(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Recur0Context extends ExprContext {
		public Recur0Context(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitRecur0(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FalseContext extends ExprContext {
		public FalseContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitFalse(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class DeclContext extends ExprContext {
		public List<TerminalNode> NAME() { return getTokens(JpParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(JpParser.NAME, i);
		}
		public DeclContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitDecl(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class NameRefContext extends ExprContext {
		public TerminalNode NAME() { return getToken(JpParser.NAME, 0); }
		public NameRefContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitNameRef(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class RangeContext extends ExprContext {
		public Token op;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RANGE() { return getToken(JpParser.RANGE, 0); }
		public TerminalNode RANGE_TO() { return getToken(JpParser.RANGE_TO, 0); }
		public RangeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitRange(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Int16Context extends ExprContext {
		public TerminalNode INT16() { return getToken(JpParser.INT16, 0); }
		public Int16Context(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitInt16(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FnCallNContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode EXTEND() { return getToken(JpParser.EXTEND, 0); }
		public FnCallNContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitFnCallN(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class VisitExprAttrContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public VisitExprAttrContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitVisitExprAttr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class IntRContext extends ExprContext {
		public TerminalNode INT_R() { return getToken(JpParser.INT_R, 0); }
		public IntRContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitIntR(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class NullContext extends ExprContext {
		public NullContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitNull(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class VecContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public VecContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitVec(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class Int10Context extends ExprContext {
		public TerminalNode INT10() { return getToken(JpParser.INT10, 0); }
		public Int10Context(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitInt10(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class CharContext extends ExprContext {
		public TerminalNode CHAR() { return getToken(JpParser.CHAR, 0); }
		public CharContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitChar(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class TrueContext extends ExprContext {
		public TrueContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitTrue(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class BlockContext extends ExprContext {
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public BlockContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class PowContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode POW() { return getToken(JpParser.POW, 0); }
		public PowContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitPow(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class SignContext extends ExprContext {
		public Token op;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ADD() { return getToken(JpParser.ADD, 0); }
		public TerminalNode SUB() { return getToken(JpParser.SUB, 0); }
		public SignContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitSign(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class IfContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public IfContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitIf(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class MapContext extends ExprContext {
		public KvPairsContext kvPairs() {
			return getRuleContext(KvPairsContext.class,0);
		}
		public MapContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitMap(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 4;
		enterRecursionRule(_localctx, 4, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				{
				_localctx = new DeclContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(30);
				match(T__1);
				setState(31);
				match(NAME);
				setState(36);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(32);
						match(T__2);
						setState(33);
						match(NAME);
						}
						} 
					}
					setState(38);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
				}
				}
				break;
			case 2:
				{
				_localctx = new DefContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(39);
				match(T__3);
				setState(40);
				struct();
				setState(41);
				match(T__4);
				setState(42);
				expr(0);
				setState(50);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(43);
						match(T__2);
						setState(44);
						struct();
						setState(45);
						match(T__4);
						setState(46);
						expr(0);
						}
						} 
					}
					setState(52);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				}
				}
				break;
			case 3:
				{
				_localctx = new TryContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(53);
				match(T__7);
				setState(54);
				expr(0);
				setState(58);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
				case 1:
					{
					setState(55);
					match(T__8);
					setState(56);
					match(NAME);
					setState(57);
					expr(0);
					}
					break;
				}
				setState(62);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
				case 1:
					{
					setState(60);
					match(T__9);
					setState(61);
					expr(0);
					}
					break;
				}
				}
				break;
			case 4:
				{
				_localctx = new ThrowContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(64);
				match(T__10);
				setState(65);
				expr(40);
				}
				break;
			case 5:
				{
				_localctx = new IfContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(66);
				match(T__11);
				setState(67);
				match(T__5);
				setState(68);
				expr(0);
				setState(69);
				match(T__2);
				setState(70);
				expr(0);
				setState(73);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__2) {
					{
					setState(71);
					match(T__2);
					setState(72);
					expr(0);
					}
				}

				setState(75);
				match(T__6);
				}
				break;
			case 6:
				{
				_localctx = new CondContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(77);
				match(T__12);
				setState(78);
				match(T__5);
				{
				setState(79);
				expr(0);
				setState(80);
				match(T__2);
				setState(81);
				expr(0);
				}
				setState(90);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(83);
					match(T__2);
					setState(84);
					expr(0);
					setState(85);
					match(T__2);
					setState(86);
					expr(0);
					}
					}
					setState(92);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(93);
				match(T__6);
				}
				break;
			case 7:
				{
				_localctx = new LoopContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(95);
				match(T__13);
				setState(96);
				match(T__5);
				setState(110);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 4611686018427715584L) != 0)) {
					{
					setState(97);
					structDefault();
					setState(98);
					match(T__2);
					setState(99);
					expr(0);
					setState(107);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__2) {
						{
						{
						setState(100);
						match(T__2);
						setState(101);
						structDefault();
						setState(102);
						match(T__2);
						setState(103);
						expr(0);
						}
						}
						setState(109);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(112);
				match(T__6);
				setState(113);
				expr(37);
				}
				break;
			case 8:
				{
				_localctx = new Recur0Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(114);
				match(T__14);
				setState(115);
				match(T__5);
				setState(116);
				match(T__6);
				}
				break;
			case 9:
				{
				_localctx = new RecurNContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(117);
				match(T__14);
				setState(118);
				match(T__5);
				setState(119);
				expr(0);
				setState(124);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(120);
					match(T__2);
					setState(121);
					expr(0);
					}
					}
					setState(126);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(127);
				match(T__6);
				}
				break;
			case 10:
				{
				_localctx = new EmptyMapContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(129);
				match(T__15);
				setState(130);
				match(T__16);
				}
				break;
			case 11:
				{
				_localctx = new MapContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(131);
				match(T__15);
				setState(132);
				kvPairs();
				setState(133);
				match(T__16);
				}
				break;
			case 12:
				{
				_localctx = new EmptyVecContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(135);
				match(T__17);
				setState(136);
				match(T__18);
				}
				break;
			case 13:
				{
				_localctx = new VecContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(137);
				match(T__17);
				setState(138);
				expr(0);
				setState(143);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(139);
						match(T__2);
						setState(140);
						expr(0);
						}
						} 
					}
					setState(145);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				}
				setState(147);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__2) {
					{
					setState(146);
					match(T__2);
					}
				}

				setState(149);
				match(T__18);
				}
				break;
			case 14:
				{
				_localctx = new TmplStrContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(151);
				match(TMPL_STRING);
				}
				break;
			case 15:
				{
				_localctx = new BracketContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(152);
				match(T__5);
				setState(153);
				expr(0);
				setState(154);
				match(T__6);
				}
				break;
			case 16:
				{
				_localctx = new PreContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(156);
				((PreContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 53188874993664L) != 0)) ) {
					((PreContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(157);
				expr(26);
				}
				break;
			case 17:
				{
				_localctx = new CharContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(158);
				match(CHAR);
				}
				break;
			case 18:
				{
				_localctx = new StringContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(159);
				str();
				}
				break;
			case 19:
				{
				_localctx = new ZeroContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(160);
				match(T__20);
				}
				break;
			case 20:
				{
				_localctx = new Int10Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(161);
				match(INT10);
				}
				break;
			case 21:
				{
				_localctx = new Int16Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(162);
				match(INT16);
				}
				break;
			case 22:
				{
				_localctx = new IntRContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(163);
				match(INT_R);
				}
				break;
			case 23:
				{
				_localctx = new FloatContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(164);
				match(FLOAT);
				}
				break;
			case 24:
				{
				_localctx = new TrueContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(165);
				match(T__21);
				}
				break;
			case 25:
				{
				_localctx = new FalseContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(166);
				match(T__22);
				}
				break;
			case 26:
				{
				_localctx = new NullContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(167);
				match(T__23);
				}
				break;
			case 27:
				{
				_localctx = new UnderlineContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(168);
				match(T__24);
				}
				break;
			case 28:
				{
				_localctx = new NameRefContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(169);
				match(NAME);
				}
				break;
			case 29:
				{
				_localctx = new BlockContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(170);
				match(T__25);
				setState(171);
				match(T__5);
				setState(175);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 2)) & ~0x3f) == 0 && ((1L << (_la - 2)) & 8070463862819356245L) != 0)) {
					{
					{
					setState(172);
					stmt();
					}
					}
					setState(177);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(178);
				match(T__6);
				}
				break;
			case 30:
				{
				_localctx = new Fn0Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(179);
				match(T__26);
				setState(182);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXTEND) {
					{
					setState(180);
					match(EXTEND);
					setState(181);
					struct();
					}
				}

				setState(184);
				match(T__6);
				setState(186);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
				case 1:
					{
					setState(185);
					str();
					}
					break;
				}
				setState(188);
				expr(3);
				}
				break;
			case 31:
				{
				_localctx = new FnNContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(189);
				match(T__26);
				setState(191);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 4611686018427715584L) != 0)) {
					{
					setState(190);
					structDefault();
					}
				}

				setState(199);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(193);
						match(T__2);
						setState(195);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 4611686018427715584L) != 0)) {
							{
							setState(194);
							structDefault();
							}
						}

						}
						} 
					}
					setState(201);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
				}
				setState(205);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__2) {
					{
					setState(202);
					match(T__2);
					setState(203);
					match(EXTEND);
					setState(204);
					struct();
					}
				}

				setState(207);
				match(T__6);
				setState(209);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
				case 1:
					{
					setState(208);
					str();
					}
					break;
				}
				setState(211);
				expr(2);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(278);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(276);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
					case 1:
						{
						_localctx = new PowContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(214);
						if (!(precpred(_ctx, 25))) throw new FailedPredicateException(this, "precpred(_ctx, 25)");
						setState(215);
						match(POW);
						setState(216);
						expr(26);
						}
						break;
					case 2:
						{
						_localctx = new AritBinContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(217);
						if (!(precpred(_ctx, 24))) throw new FailedPredicateException(this, "precpred(_ctx, 24)");
						setState(218);
						((AritBinContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 15393162788864L) != 0)) ) {
							((AritBinContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(219);
						expr(25);
						}
						break;
					case 3:
						{
						_localctx = new SignContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(220);
						if (!(precpred(_ctx, 23))) throw new FailedPredicateException(this, "precpred(_ctx, 23)");
						setState(221);
						((SignContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==ADD || _la==SUB) ) {
							((SignContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(222);
						expr(24);
						}
						break;
					case 4:
						{
						_localctx = new ShiftContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(223);
						if (!(precpred(_ctx, 22))) throw new FailedPredicateException(this, "precpred(_ctx, 22)");
						setState(224);
						((ShiftContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 492581209243648L) != 0)) ) {
							((ShiftContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(225);
						expr(23);
						}
						break;
					case 5:
						{
						_localctx = new CompContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(226);
						if (!(precpred(_ctx, 21))) throw new FailedPredicateException(this, "precpred(_ctx, 21)");
						setState(227);
						((CompContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 8444249301319680L) != 0)) ) {
							((CompContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(228);
						expr(22);
						}
						break;
					case 6:
						{
						_localctx = new EqualContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(229);
						if (!(precpred(_ctx, 20))) throw new FailedPredicateException(this, "precpred(_ctx, 20)");
						setState(230);
						((EqualContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==EQ || _la==NOT_EQ) ) {
							((EqualContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(231);
						expr(21);
						}
						break;
					case 7:
						{
						_localctx = new BitLogicContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(232);
						if (!(precpred(_ctx, 19))) throw new FailedPredicateException(this, "precpred(_ctx, 19)");
						setState(233);
						((BitLogicContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 252201579132747776L) != 0)) ) {
							((BitLogicContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(234);
						expr(20);
						}
						break;
					case 8:
						{
						_localctx = new LogicContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(235);
						if (!(precpred(_ctx, 18))) throw new FailedPredicateException(this, "precpred(_ctx, 18)");
						setState(236);
						((LogicContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==AND || _la==OR) ) {
							((LogicContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(237);
						expr(19);
						}
						break;
					case 9:
						{
						_localctx = new RangeContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(238);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(239);
						((RangeContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==RANGE || _la==RANGE_TO) ) {
							((RangeContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(240);
						expr(18);
						}
						break;
					case 10:
						{
						_localctx = new PipContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(241);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(242);
						match(T__27);
						setState(243);
						expr(2);
						}
						break;
					case 11:
						{
						_localctx = new FnCall0Context(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(244);
						if (!(precpred(_ctx, 43))) throw new FailedPredicateException(this, "precpred(_ctx, 43)");
						setState(245);
						match(T__5);
						setState(248);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==EXTEND) {
							{
							setState(246);
							match(EXTEND);
							setState(247);
							expr(0);
							}
						}

						setState(250);
						match(T__6);
						}
						break;
					case 12:
						{
						_localctx = new FnCallNContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(251);
						if (!(precpred(_ctx, 42))) throw new FailedPredicateException(this, "precpred(_ctx, 42)");
						setState(252);
						match(T__5);
						setState(253);
						expr(0);
						setState(258);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(254);
								match(T__2);
								setState(255);
								expr(0);
								}
								} 
							}
							setState(260);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
						}
						setState(264);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==T__2) {
							{
							setState(261);
							match(T__2);
							setState(262);
							match(EXTEND);
							setState(263);
							expr(0);
							}
						}

						setState(266);
						match(T__6);
						}
						break;
					case 13:
						{
						_localctx = new VisitAttrContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(268);
						if (!(precpred(_ctx, 28))) throw new FailedPredicateException(this, "precpred(_ctx, 28)");
						setState(269);
						match(T__19);
						setState(270);
						match(NAME);
						}
						break;
					case 14:
						{
						_localctx = new VisitExprAttrContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(271);
						if (!(precpred(_ctx, 27))) throw new FailedPredicateException(this, "precpred(_ctx, 27)");
						setState(272);
						match(T__17);
						setState(273);
						expr(0);
						setState(274);
						match(T__18);
						}
						break;
					}
					} 
				}
				setState(280);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StructContext extends ParserRuleContext {
		public StructContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct; }
	 
		public StructContext() { }
		public void copyFrom(StructContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class MapStructNContext extends StructContext {
		public List<StructPairContext> structPair() {
			return getRuleContexts(StructPairContext.class);
		}
		public StructPairContext structPair(int i) {
			return getRuleContext(StructPairContext.class,i);
		}
		public AsContext as() {
			return getRuleContext(AsContext.class,0);
		}
		public TerminalNode EXTEND() { return getToken(JpParser.EXTEND, 0); }
		public StructContext struct() {
			return getRuleContext(StructContext.class,0);
		}
		public MapStructNContext(StructContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitMapStructN(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class VecStructNContext extends StructContext {
		public List<StructDefaultContext> structDefault() {
			return getRuleContexts(StructDefaultContext.class);
		}
		public StructDefaultContext structDefault(int i) {
			return getRuleContext(StructDefaultContext.class,i);
		}
		public AsContext as() {
			return getRuleContext(AsContext.class,0);
		}
		public TerminalNode EXTEND() { return getToken(JpParser.EXTEND, 0); }
		public StructContext struct() {
			return getRuleContext(StructContext.class,0);
		}
		public VecStructNContext(StructContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitVecStructN(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class MapStruct0Context extends StructContext {
		public TerminalNode EXTEND() { return getToken(JpParser.EXTEND, 0); }
		public StructContext struct() {
			return getRuleContext(StructContext.class,0);
		}
		public AsContext as() {
			return getRuleContext(AsContext.class,0);
		}
		public MapStruct0Context(StructContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitMapStruct0(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class VecStruct0Context extends StructContext {
		public TerminalNode EXTEND() { return getToken(JpParser.EXTEND, 0); }
		public StructContext struct() {
			return getRuleContext(StructContext.class,0);
		}
		public AsContext as() {
			return getRuleContext(AsContext.class,0);
		}
		public VecStruct0Context(StructContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitVecStruct0(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class NameStructContext extends StructContext {
		public TerminalNode NAME() { return getToken(JpParser.NAME, 0); }
		public NameStructContext(StructContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitNameStruct(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StructContext struct() throws RecognitionException {
		StructContext _localctx = new StructContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_struct);
		int _la;
		try {
			int _alt;
			setState(340);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				_localctx = new NameStructContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(281);
				match(NAME);
				}
				break;
			case 2:
				_localctx = new VecStruct0Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(282);
				match(T__17);
				setState(285);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXTEND) {
					{
					setState(283);
					match(EXTEND);
					setState(284);
					struct();
					}
				}

				setState(288);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__29) {
					{
					setState(287);
					as();
					}
				}

				setState(290);
				match(T__18);
				}
				break;
			case 3:
				_localctx = new VecStructNContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(291);
				match(T__17);
				setState(292);
				structDefault();
				setState(297);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(293);
						match(T__2);
						setState(294);
						structDefault();
						}
						} 
					}
					setState(299);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
				}
				setState(304);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
				case 1:
					{
					setState(300);
					match(T__2);
					}
					break;
				case 2:
					{
					{
					setState(301);
					match(T__2);
					setState(302);
					match(EXTEND);
					setState(303);
					struct();
					}
					}
					break;
				}
				setState(307);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__29) {
					{
					setState(306);
					as();
					}
				}

				setState(309);
				match(T__18);
				}
				break;
			case 4:
				_localctx = new MapStruct0Context(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(311);
				match(T__15);
				setState(314);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXTEND) {
					{
					setState(312);
					match(EXTEND);
					setState(313);
					struct();
					}
				}

				setState(317);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__29) {
					{
					setState(316);
					as();
					}
				}

				setState(319);
				match(T__16);
				}
				break;
			case 5:
				_localctx = new MapStructNContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(320);
				match(T__15);
				setState(321);
				structPair();
				setState(326);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(322);
						match(T__2);
						setState(323);
						structPair();
						}
						} 
					}
					setState(328);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
				}
				setState(333);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
				case 1:
					{
					setState(329);
					match(T__2);
					}
					break;
				case 2:
					{
					{
					setState(330);
					match(T__2);
					setState(331);
					match(EXTEND);
					setState(332);
					struct();
					}
					}
					break;
				}
				setState(336);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__29) {
					{
					setState(335);
					as();
					}
				}

				setState(338);
				match(T__16);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StructPairContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(JpParser.NAME, 0); }
		public StructDefaultContext structDefault() {
			return getRuleContext(StructDefaultContext.class,0);
		}
		public StructPairContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structPair; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitStructPair(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StructPairContext structPair() throws RecognitionException {
		StructPairContext _localctx = new StructPairContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_structPair);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(342);
			match(NAME);
			setState(345);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__28) {
				{
				setState(343);
				match(T__28);
				setState(344);
				structDefault();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StructDefaultContext extends ParserRuleContext {
		public StructContext struct() {
			return getRuleContext(StructContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StructDefaultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structDefault; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitStructDefault(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StructDefaultContext structDefault() throws RecognitionException {
		StructDefaultContext _localctx = new StructDefaultContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_structDefault);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(347);
			struct();
			setState(350);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__4) {
				{
				setState(348);
				match(T__4);
				setState(349);
				expr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class AsContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(JpParser.NAME, 0); }
		public AsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_as; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitAs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AsContext as() throws RecognitionException {
		AsContext _localctx = new AsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_as);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(352);
			match(T__29);
			setState(353);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StrContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(JpParser.STRING, 0); }
		public StrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_str; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitStr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StrContext str() throws RecognitionException {
		StrContext _localctx = new StrContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_str);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(355);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class KvPairsContext extends ParserRuleContext {
		public List<KvPairContext> kvPair() {
			return getRuleContexts(KvPairContext.class);
		}
		public KvPairContext kvPair(int i) {
			return getRuleContext(KvPairContext.class,i);
		}
		public KvPairsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kvPairs; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitKvPairs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KvPairsContext kvPairs() throws RecognitionException {
		KvPairsContext _localctx = new KvPairsContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_kvPairs);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(357);
			kvPair();
			setState(362);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(358);
					match(T__2);
					setState(359);
					kvPair();
					}
					} 
				}
				setState(364);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			}
			setState(366);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__2) {
				{
				setState(365);
				match(T__2);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class KvPairContext extends ParserRuleContext {
		public KvPairContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kvPair; }
	 
		public KvPairContext() { }
		public void copyFrom(KvPairContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class NamePairContext extends KvPairContext {
		public TerminalNode NAME() { return getToken(JpParser.NAME, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public NamePairContext(KvPairContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitNamePair(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class StrPairContext extends KvPairContext {
		public StrContext str() {
			return getRuleContext(StrContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StrPairContext(KvPairContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitStrPair(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExprPairContext extends KvPairContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ExprPairContext(KvPairContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JpVisitor ) return ((JpVisitor<? extends T>)visitor).visitExprPair(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KvPairContext kvPair() throws RecognitionException {
		KvPairContext _localctx = new KvPairContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_kvPair);
		try {
			setState(380);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NAME:
				_localctx = new NamePairContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(368);
				match(NAME);
				setState(369);
				match(T__28);
				setState(370);
				expr(0);
				}
				break;
			case STRING:
				_localctx = new StrPairContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(371);
				str();
				setState(372);
				match(T__28);
				setState(373);
				expr(0);
				}
				break;
			case T__5:
				_localctx = new ExprPairContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(375);
				match(T__5);
				setState(376);
				expr(0);
				setState(377);
				match(T__30);
				setState(378);
				expr(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 2:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 25);
		case 1:
			return precpred(_ctx, 24);
		case 2:
			return precpred(_ctx, 23);
		case 3:
			return precpred(_ctx, 22);
		case 4:
			return precpred(_ctx, 21);
		case 5:
			return precpred(_ctx, 20);
		case 6:
			return precpred(_ctx, 19);
		case 7:
			return precpred(_ctx, 18);
		case 8:
			return precpred(_ctx, 17);
		case 9:
			return precpred(_ctx, 1);
		case 10:
			return precpred(_ctx, 43);
		case 11:
			return precpred(_ctx, 42);
		case 12:
			return precpred(_ctx, 28);
		case 13:
			return precpred(_ctx, 27);
		}
		return true;
	}

	public static final String _serializedATN =
		"\u0004\u0001C\u017f\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004\u0002"+
		"\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007\u0002"+
		"\b\u0007\b\u0002\t\u0007\t\u0001\u0000\u0005\u0000\u0016\b\u0000\n\u0000"+
		"\f\u0000\u0019\t\u0000\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0005\u0002#\b\u0002"+
		"\n\u0002\f\u0002&\t\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0005\u0002"+
		"1\b\u0002\n\u0002\f\u00024\t\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0003\u0002;\b\u0002\u0001\u0002\u0001\u0002"+
		"\u0003\u0002?\b\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0003\u0002"+
		"J\b\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0005\u0002Y\b\u0002\n\u0002\f\u0002\\\t\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0005\u0002j\b\u0002\n\u0002\f\u0002m\t\u0002\u0003\u0002o\b\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0005\u0002{\b\u0002\n\u0002"+
		"\f\u0002~\t\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0005\u0002\u008e\b\u0002\n"+
		"\u0002\f\u0002\u0091\t\u0002\u0001\u0002\u0003\u0002\u0094\b\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0005"+
		"\u0002\u00ae\b\u0002\n\u0002\f\u0002\u00b1\t\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0003\u0002\u00b7\b\u0002\u0001\u0002\u0001\u0002"+
		"\u0003\u0002\u00bb\b\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0003\u0002"+
		"\u00c0\b\u0002\u0001\u0002\u0001\u0002\u0003\u0002\u00c4\b\u0002\u0005"+
		"\u0002\u00c6\b\u0002\n\u0002\f\u0002\u00c9\t\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0003\u0002\u00ce\b\u0002\u0001\u0002\u0001\u0002\u0003\u0002"+
		"\u00d2\b\u0002\u0001\u0002\u0003\u0002\u00d5\b\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0003\u0002\u00f9\b\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0005\u0002\u0101"+
		"\b\u0002\n\u0002\f\u0002\u0104\t\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0003\u0002\u0109\b\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0005\u0002\u0115\b\u0002\n\u0002\f\u0002\u0118\t\u0002\u0001\u0003\u0001"+
		"\u0003\u0001\u0003\u0001\u0003\u0003\u0003\u011e\b\u0003\u0001\u0003\u0003"+
		"\u0003\u0121\b\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001"+
		"\u0003\u0005\u0003\u0128\b\u0003\n\u0003\f\u0003\u012b\t\u0003\u0001\u0003"+
		"\u0001\u0003\u0001\u0003\u0001\u0003\u0003\u0003\u0131\b\u0003\u0001\u0003"+
		"\u0003\u0003\u0134\b\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003"+
		"\u0001\u0003\u0003\u0003\u013b\b\u0003\u0001\u0003\u0003\u0003\u013e\b"+
		"\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0005"+
		"\u0003\u0145\b\u0003\n\u0003\f\u0003\u0148\t\u0003\u0001\u0003\u0001\u0003"+
		"\u0001\u0003\u0001\u0003\u0003\u0003\u014e\b\u0003\u0001\u0003\u0003\u0003"+
		"\u0151\b\u0003\u0001\u0003\u0001\u0003\u0003\u0003\u0155\b\u0003\u0001"+
		"\u0004\u0001\u0004\u0001\u0004\u0003\u0004\u015a\b\u0004\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0003\u0005\u015f\b\u0005\u0001\u0006\u0001\u0006\u0001"+
		"\u0006\u0001\u0007\u0001\u0007\u0001\b\u0001\b\u0001\b\u0005\b\u0169\b"+
		"\b\n\b\f\b\u016c\t\b\u0001\b\u0003\b\u016f\b\b\u0001\t\u0001\t\u0001\t"+
		"\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001"+
		"\t\u0003\t\u017d\b\t\u0001\t\u0000\u0001\u0004\n\u0000\u0002\u0004\u0006"+
		"\b\n\f\u000e\u0010\u0012\u0000\t\u0002\u0000%&,-\u0001\u0000)+\u0001\u0000"+
		",-\u0001\u0000.0\u0001\u000014\u0001\u000056\u0001\u000079\u0001\u0000"+
		":;\u0001\u0000<=\u01cd\u0000\u0017\u0001\u0000\u0000\u0000\u0002\u001a"+
		"\u0001\u0000\u0000\u0000\u0004\u00d4\u0001\u0000\u0000\u0000\u0006\u0154"+
		"\u0001\u0000\u0000\u0000\b\u0156\u0001\u0000\u0000\u0000\n\u015b\u0001"+
		"\u0000\u0000\u0000\f\u0160\u0001\u0000\u0000\u0000\u000e\u0163\u0001\u0000"+
		"\u0000\u0000\u0010\u0165\u0001\u0000\u0000\u0000\u0012\u017c\u0001\u0000"+
		"\u0000\u0000\u0014\u0016\u0003\u0002\u0001\u0000\u0015\u0014\u0001\u0000"+
		"\u0000\u0000\u0016\u0019\u0001\u0000\u0000\u0000\u0017\u0015\u0001\u0000"+
		"\u0000\u0000\u0017\u0018\u0001\u0000\u0000\u0000\u0018\u0001\u0001\u0000"+
		"\u0000\u0000\u0019\u0017\u0001\u0000\u0000\u0000\u001a\u001b\u0003\u0004"+
		"\u0002\u0000\u001b\u001c\u0005\u0001\u0000\u0000\u001c\u0003\u0001\u0000"+
		"\u0000\u0000\u001d\u001e\u0006\u0002\uffff\uffff\u0000\u001e\u001f\u0005"+
		"\u0002\u0000\u0000\u001f$\u0005>\u0000\u0000 !\u0005\u0003\u0000\u0000"+
		"!#\u0005>\u0000\u0000\" \u0001\u0000\u0000\u0000#&\u0001\u0000\u0000\u0000"+
		"$\"\u0001\u0000\u0000\u0000$%\u0001\u0000\u0000\u0000%\u00d5\u0001\u0000"+
		"\u0000\u0000&$\u0001\u0000\u0000\u0000\'(\u0005\u0004\u0000\u0000()\u0003"+
		"\u0006\u0003\u0000)*\u0005\u0005\u0000\u0000*2\u0003\u0004\u0002\u0000"+
		"+,\u0005\u0003\u0000\u0000,-\u0003\u0006\u0003\u0000-.\u0005\u0005\u0000"+
		"\u0000./\u0003\u0004\u0002\u0000/1\u0001\u0000\u0000\u00000+\u0001\u0000"+
		"\u0000\u000014\u0001\u0000\u0000\u000020\u0001\u0000\u0000\u000023\u0001"+
		"\u0000\u0000\u00003\u00d5\u0001\u0000\u0000\u000042\u0001\u0000\u0000"+
		"\u000056\u0005\b\u0000\u00006:\u0003\u0004\u0002\u000078\u0005\t\u0000"+
		"\u000089\u0005>\u0000\u00009;\u0003\u0004\u0002\u0000:7\u0001\u0000\u0000"+
		"\u0000:;\u0001\u0000\u0000\u0000;>\u0001\u0000\u0000\u0000<=\u0005\n\u0000"+
		"\u0000=?\u0003\u0004\u0002\u0000><\u0001\u0000\u0000\u0000>?\u0001\u0000"+
		"\u0000\u0000?\u00d5\u0001\u0000\u0000\u0000@A\u0005\u000b\u0000\u0000"+
		"A\u00d5\u0003\u0004\u0002(BC\u0005\f\u0000\u0000CD\u0005\u0006\u0000\u0000"+
		"DE\u0003\u0004\u0002\u0000EF\u0005\u0003\u0000\u0000FI\u0003\u0004\u0002"+
		"\u0000GH\u0005\u0003\u0000\u0000HJ\u0003\u0004\u0002\u0000IG\u0001\u0000"+
		"\u0000\u0000IJ\u0001\u0000\u0000\u0000JK\u0001\u0000\u0000\u0000KL\u0005"+
		"\u0007\u0000\u0000L\u00d5\u0001\u0000\u0000\u0000MN\u0005\r\u0000\u0000"+
		"NO\u0005\u0006\u0000\u0000OP\u0003\u0004\u0002\u0000PQ\u0005\u0003\u0000"+
		"\u0000QR\u0003\u0004\u0002\u0000RZ\u0001\u0000\u0000\u0000ST\u0005\u0003"+
		"\u0000\u0000TU\u0003\u0004\u0002\u0000UV\u0005\u0003\u0000\u0000VW\u0003"+
		"\u0004\u0002\u0000WY\u0001\u0000\u0000\u0000XS\u0001\u0000\u0000\u0000"+
		"Y\\\u0001\u0000\u0000\u0000ZX\u0001\u0000\u0000\u0000Z[\u0001\u0000\u0000"+
		"\u0000[]\u0001\u0000\u0000\u0000\\Z\u0001\u0000\u0000\u0000]^\u0005\u0007"+
		"\u0000\u0000^\u00d5\u0001\u0000\u0000\u0000_`\u0005\u000e\u0000\u0000"+
		"`n\u0005\u0006\u0000\u0000ab\u0003\n\u0005\u0000bc\u0005\u0003\u0000\u0000"+
		"ck\u0003\u0004\u0002\u0000de\u0005\u0003\u0000\u0000ef\u0003\n\u0005\u0000"+
		"fg\u0005\u0003\u0000\u0000gh\u0003\u0004\u0002\u0000hj\u0001\u0000\u0000"+
		"\u0000id\u0001\u0000\u0000\u0000jm\u0001\u0000\u0000\u0000ki\u0001\u0000"+
		"\u0000\u0000kl\u0001\u0000\u0000\u0000lo\u0001\u0000\u0000\u0000mk\u0001"+
		"\u0000\u0000\u0000na\u0001\u0000\u0000\u0000no\u0001\u0000\u0000\u0000"+
		"op\u0001\u0000\u0000\u0000pq\u0005\u0007\u0000\u0000q\u00d5\u0003\u0004"+
		"\u0002%rs\u0005\u000f\u0000\u0000st\u0005\u0006\u0000\u0000t\u00d5\u0005"+
		"\u0007\u0000\u0000uv\u0005\u000f\u0000\u0000vw\u0005\u0006\u0000\u0000"+
		"w|\u0003\u0004\u0002\u0000xy\u0005\u0003\u0000\u0000y{\u0003\u0004\u0002"+
		"\u0000zx\u0001\u0000\u0000\u0000{~\u0001\u0000\u0000\u0000|z\u0001\u0000"+
		"\u0000\u0000|}\u0001\u0000\u0000\u0000}\u007f\u0001\u0000\u0000\u0000"+
		"~|\u0001\u0000\u0000\u0000\u007f\u0080\u0005\u0007\u0000\u0000\u0080\u00d5"+
		"\u0001\u0000\u0000\u0000\u0081\u0082\u0005\u0010\u0000\u0000\u0082\u00d5"+
		"\u0005\u0011\u0000\u0000\u0083\u0084\u0005\u0010\u0000\u0000\u0084\u0085"+
		"\u0003\u0010\b\u0000\u0085\u0086\u0005\u0011\u0000\u0000\u0086\u00d5\u0001"+
		"\u0000\u0000\u0000\u0087\u0088\u0005\u0012\u0000\u0000\u0088\u00d5\u0005"+
		"\u0013\u0000\u0000\u0089\u008a\u0005\u0012\u0000\u0000\u008a\u008f\u0003"+
		"\u0004\u0002\u0000\u008b\u008c\u0005\u0003\u0000\u0000\u008c\u008e\u0003"+
		"\u0004\u0002\u0000\u008d\u008b\u0001\u0000\u0000\u0000\u008e\u0091\u0001"+
		"\u0000\u0000\u0000\u008f\u008d\u0001\u0000\u0000\u0000\u008f\u0090\u0001"+
		"\u0000\u0000\u0000\u0090\u0093\u0001\u0000\u0000\u0000\u0091\u008f\u0001"+
		"\u0000\u0000\u0000\u0092\u0094\u0005\u0003\u0000\u0000\u0093\u0092\u0001"+
		"\u0000\u0000\u0000\u0093\u0094\u0001\u0000\u0000\u0000\u0094\u0095\u0001"+
		"\u0000\u0000\u0000\u0095\u0096\u0005\u0013\u0000\u0000\u0096\u00d5\u0001"+
		"\u0000\u0000\u0000\u0097\u00d5\u0005@\u0000\u0000\u0098\u0099\u0005\u0006"+
		"\u0000\u0000\u0099\u009a\u0003\u0004\u0002\u0000\u009a\u009b\u0005\u0007"+
		"\u0000\u0000\u009b\u00d5\u0001\u0000\u0000\u0000\u009c\u009d\u0007\u0000"+
		"\u0000\u0000\u009d\u00d5\u0003\u0004\u0002\u001a\u009e\u00d5\u0005 \u0000"+
		"\u0000\u009f\u00d5\u0003\u000e\u0007\u0000\u00a0\u00d5\u0005\u0015\u0000"+
		"\u0000\u00a1\u00d5\u0005!\u0000\u0000\u00a2\u00d5\u0005\"\u0000\u0000"+
		"\u00a3\u00d5\u0005#\u0000\u0000\u00a4\u00d5\u0005$\u0000\u0000\u00a5\u00d5"+
		"\u0005\u0016\u0000\u0000\u00a6\u00d5\u0005\u0017\u0000\u0000\u00a7\u00d5"+
		"\u0005\u0018\u0000\u0000\u00a8\u00d5\u0005\u0019\u0000\u0000\u00a9\u00d5"+
		"\u0005>\u0000\u0000\u00aa\u00ab\u0005\u001a\u0000\u0000\u00ab\u00af\u0005"+
		"\u0006\u0000\u0000\u00ac\u00ae\u0003\u0002\u0001\u0000\u00ad\u00ac\u0001"+
		"\u0000\u0000\u0000\u00ae\u00b1\u0001\u0000\u0000\u0000\u00af\u00ad\u0001"+
		"\u0000\u0000\u0000\u00af\u00b0\u0001\u0000\u0000\u0000\u00b0\u00b2\u0001"+
		"\u0000\u0000\u0000\u00b1\u00af\u0001\u0000\u0000\u0000\u00b2\u00d5\u0005"+
		"\u0007\u0000\u0000\u00b3\u00b6\u0005\u001b\u0000\u0000\u00b4\u00b5\u0005"+
		"\'\u0000\u0000\u00b5\u00b7\u0003\u0006\u0003\u0000\u00b6\u00b4\u0001\u0000"+
		"\u0000\u0000\u00b6\u00b7\u0001\u0000\u0000\u0000\u00b7\u00b8\u0001\u0000"+
		"\u0000\u0000\u00b8\u00ba\u0005\u0007\u0000\u0000\u00b9\u00bb\u0003\u000e"+
		"\u0007\u0000\u00ba\u00b9\u0001\u0000\u0000\u0000\u00ba\u00bb\u0001\u0000"+
		"\u0000\u0000\u00bb\u00bc\u0001\u0000\u0000\u0000\u00bc\u00d5\u0003\u0004"+
		"\u0002\u0003\u00bd\u00bf\u0005\u001b\u0000\u0000\u00be\u00c0\u0003\n\u0005"+
		"\u0000\u00bf\u00be\u0001\u0000\u0000\u0000\u00bf\u00c0\u0001\u0000\u0000"+
		"\u0000\u00c0\u00c7\u0001\u0000\u0000\u0000\u00c1\u00c3\u0005\u0003\u0000"+
		"\u0000\u00c2\u00c4\u0003\n\u0005\u0000\u00c3\u00c2\u0001\u0000\u0000\u0000"+
		"\u00c3\u00c4\u0001\u0000\u0000\u0000\u00c4\u00c6\u0001\u0000\u0000\u0000"+
		"\u00c5\u00c1\u0001\u0000\u0000\u0000\u00c6\u00c9\u0001\u0000\u0000\u0000"+
		"\u00c7\u00c5\u0001\u0000\u0000\u0000\u00c7\u00c8\u0001\u0000\u0000\u0000"+
		"\u00c8\u00cd\u0001\u0000\u0000\u0000\u00c9\u00c7\u0001\u0000\u0000\u0000"+
		"\u00ca\u00cb\u0005\u0003\u0000\u0000\u00cb\u00cc\u0005\'\u0000\u0000\u00cc"+
		"\u00ce\u0003\u0006\u0003\u0000\u00cd\u00ca\u0001\u0000\u0000\u0000\u00cd"+
		"\u00ce\u0001\u0000\u0000\u0000\u00ce\u00cf\u0001\u0000\u0000\u0000\u00cf"+
		"\u00d1\u0005\u0007\u0000\u0000\u00d0\u00d2\u0003\u000e\u0007\u0000\u00d1"+
		"\u00d0\u0001\u0000\u0000\u0000\u00d1\u00d2\u0001\u0000\u0000\u0000\u00d2"+
		"\u00d3\u0001\u0000\u0000\u0000\u00d3\u00d5\u0003\u0004\u0002\u0002\u00d4"+
		"\u001d\u0001\u0000\u0000\u0000\u00d4\'\u0001\u0000\u0000\u0000\u00d45"+
		"\u0001\u0000\u0000\u0000\u00d4@\u0001\u0000\u0000\u0000\u00d4B\u0001\u0000"+
		"\u0000\u0000\u00d4M\u0001\u0000\u0000\u0000\u00d4_\u0001\u0000\u0000\u0000"+
		"\u00d4r\u0001\u0000\u0000\u0000\u00d4u\u0001\u0000\u0000\u0000\u00d4\u0081"+
		"\u0001\u0000\u0000\u0000\u00d4\u0083\u0001\u0000\u0000\u0000\u00d4\u0087"+
		"\u0001\u0000\u0000\u0000\u00d4\u0089\u0001\u0000\u0000\u0000\u00d4\u0097"+
		"\u0001\u0000\u0000\u0000\u00d4\u0098\u0001\u0000\u0000\u0000\u00d4\u009c"+
		"\u0001\u0000\u0000\u0000\u00d4\u009e\u0001\u0000\u0000\u0000\u00d4\u009f"+
		"\u0001\u0000\u0000\u0000\u00d4\u00a0\u0001\u0000\u0000\u0000\u00d4\u00a1"+
		"\u0001\u0000\u0000\u0000\u00d4\u00a2\u0001\u0000\u0000\u0000\u00d4\u00a3"+
		"\u0001\u0000\u0000\u0000\u00d4\u00a4\u0001\u0000\u0000\u0000\u00d4\u00a5"+
		"\u0001\u0000\u0000\u0000\u00d4\u00a6\u0001\u0000\u0000\u0000\u00d4\u00a7"+
		"\u0001\u0000\u0000\u0000\u00d4\u00a8\u0001\u0000\u0000\u0000\u00d4\u00a9"+
		"\u0001\u0000\u0000\u0000\u00d4\u00aa\u0001\u0000\u0000\u0000\u00d4\u00b3"+
		"\u0001\u0000\u0000\u0000\u00d4\u00bd\u0001\u0000\u0000\u0000\u00d5\u0116"+
		"\u0001\u0000\u0000\u0000\u00d6\u00d7\n\u0019\u0000\u0000\u00d7\u00d8\u0005"+
		"(\u0000\u0000\u00d8\u0115\u0003\u0004\u0002\u001a\u00d9\u00da\n\u0018"+
		"\u0000\u0000\u00da\u00db\u0007\u0001\u0000\u0000\u00db\u0115\u0003\u0004"+
		"\u0002\u0019\u00dc\u00dd\n\u0017\u0000\u0000\u00dd\u00de\u0007\u0002\u0000"+
		"\u0000\u00de\u0115\u0003\u0004\u0002\u0018\u00df\u00e0\n\u0016\u0000\u0000"+
		"\u00e0\u00e1\u0007\u0003\u0000\u0000\u00e1\u0115\u0003\u0004\u0002\u0017"+
		"\u00e2\u00e3\n\u0015\u0000\u0000\u00e3\u00e4\u0007\u0004\u0000\u0000\u00e4"+
		"\u0115\u0003\u0004\u0002\u0016\u00e5\u00e6\n\u0014\u0000\u0000\u00e6\u00e7"+
		"\u0007\u0005\u0000\u0000\u00e7\u0115\u0003\u0004\u0002\u0015\u00e8\u00e9"+
		"\n\u0013\u0000\u0000\u00e9\u00ea\u0007\u0006\u0000\u0000\u00ea\u0115\u0003"+
		"\u0004\u0002\u0014\u00eb\u00ec\n\u0012\u0000\u0000\u00ec\u00ed\u0007\u0007"+
		"\u0000\u0000\u00ed\u0115\u0003\u0004\u0002\u0013\u00ee\u00ef\n\u0011\u0000"+
		"\u0000\u00ef\u00f0\u0007\b\u0000\u0000\u00f0\u0115\u0003\u0004\u0002\u0012"+
		"\u00f1\u00f2\n\u0001\u0000\u0000\u00f2\u00f3\u0005\u001c\u0000\u0000\u00f3"+
		"\u0115\u0003\u0004\u0002\u0002\u00f4\u00f5\n+\u0000\u0000\u00f5\u00f8"+
		"\u0005\u0006\u0000\u0000\u00f6\u00f7\u0005\'\u0000\u0000\u00f7\u00f9\u0003"+
		"\u0004\u0002\u0000\u00f8\u00f6\u0001\u0000\u0000\u0000\u00f8\u00f9\u0001"+
		"\u0000\u0000\u0000\u00f9\u00fa\u0001\u0000\u0000\u0000\u00fa\u0115\u0005"+
		"\u0007\u0000\u0000\u00fb\u00fc\n*\u0000\u0000\u00fc\u00fd\u0005\u0006"+
		"\u0000\u0000\u00fd\u0102\u0003\u0004\u0002\u0000\u00fe\u00ff\u0005\u0003"+
		"\u0000\u0000\u00ff\u0101\u0003\u0004\u0002\u0000\u0100\u00fe\u0001\u0000"+
		"\u0000\u0000\u0101\u0104\u0001\u0000\u0000\u0000\u0102\u0100\u0001\u0000"+
		"\u0000\u0000\u0102\u0103\u0001\u0000\u0000\u0000\u0103\u0108\u0001\u0000"+
		"\u0000\u0000\u0104\u0102\u0001\u0000\u0000\u0000\u0105\u0106\u0005\u0003"+
		"\u0000\u0000\u0106\u0107\u0005\'\u0000\u0000\u0107\u0109\u0003\u0004\u0002"+
		"\u0000\u0108\u0105\u0001\u0000\u0000\u0000\u0108\u0109\u0001\u0000\u0000"+
		"\u0000\u0109\u010a\u0001\u0000\u0000\u0000\u010a\u010b\u0005\u0007\u0000"+
		"\u0000\u010b\u0115\u0001\u0000\u0000\u0000\u010c\u010d\n\u001c\u0000\u0000"+
		"\u010d\u010e\u0005\u0014\u0000\u0000\u010e\u0115\u0005>\u0000\u0000\u010f"+
		"\u0110\n\u001b\u0000\u0000\u0110\u0111\u0005\u0012\u0000\u0000\u0111\u0112"+
		"\u0003\u0004\u0002\u0000\u0112\u0113\u0005\u0013\u0000\u0000\u0113\u0115"+
		"\u0001\u0000\u0000\u0000\u0114\u00d6\u0001\u0000\u0000\u0000\u0114\u00d9"+
		"\u0001\u0000\u0000\u0000\u0114\u00dc\u0001\u0000\u0000\u0000\u0114\u00df"+
		"\u0001\u0000\u0000\u0000\u0114\u00e2\u0001\u0000\u0000\u0000\u0114\u00e5"+
		"\u0001\u0000\u0000\u0000\u0114\u00e8\u0001\u0000\u0000\u0000\u0114\u00eb"+
		"\u0001\u0000\u0000\u0000\u0114\u00ee\u0001\u0000\u0000\u0000\u0114\u00f1"+
		"\u0001\u0000\u0000\u0000\u0114\u00f4\u0001\u0000\u0000\u0000\u0114\u00fb"+
		"\u0001\u0000\u0000\u0000\u0114\u010c\u0001\u0000\u0000\u0000\u0114\u010f"+
		"\u0001\u0000\u0000\u0000\u0115\u0118\u0001\u0000\u0000\u0000\u0116\u0114"+
		"\u0001\u0000\u0000\u0000\u0116\u0117\u0001\u0000\u0000\u0000\u0117\u0005"+
		"\u0001\u0000\u0000\u0000\u0118\u0116\u0001\u0000\u0000\u0000\u0119\u0155"+
		"\u0005>\u0000\u0000\u011a\u011d\u0005\u0012\u0000\u0000\u011b\u011c\u0005"+
		"\'\u0000\u0000\u011c\u011e\u0003\u0006\u0003\u0000\u011d\u011b\u0001\u0000"+
		"\u0000\u0000\u011d\u011e\u0001\u0000\u0000\u0000\u011e\u0120\u0001\u0000"+
		"\u0000\u0000\u011f\u0121\u0003\f\u0006\u0000\u0120\u011f\u0001\u0000\u0000"+
		"\u0000\u0120\u0121\u0001\u0000\u0000\u0000\u0121\u0122\u0001\u0000\u0000"+
		"\u0000\u0122\u0155\u0005\u0013\u0000\u0000\u0123\u0124\u0005\u0012\u0000"+
		"\u0000\u0124\u0129\u0003\n\u0005\u0000\u0125\u0126\u0005\u0003\u0000\u0000"+
		"\u0126\u0128\u0003\n\u0005\u0000\u0127\u0125\u0001\u0000\u0000\u0000\u0128"+
		"\u012b\u0001\u0000\u0000\u0000\u0129\u0127\u0001\u0000\u0000\u0000\u0129"+
		"\u012a\u0001\u0000\u0000\u0000\u012a\u0130\u0001\u0000\u0000\u0000\u012b"+
		"\u0129\u0001\u0000\u0000\u0000\u012c\u0131\u0005\u0003\u0000\u0000\u012d"+
		"\u012e\u0005\u0003\u0000\u0000\u012e\u012f\u0005\'\u0000\u0000\u012f\u0131"+
		"\u0003\u0006\u0003\u0000\u0130\u012c\u0001\u0000\u0000\u0000\u0130\u012d"+
		"\u0001\u0000\u0000\u0000\u0130\u0131\u0001\u0000\u0000\u0000\u0131\u0133"+
		"\u0001\u0000\u0000\u0000\u0132\u0134\u0003\f\u0006\u0000\u0133\u0132\u0001"+
		"\u0000\u0000\u0000\u0133\u0134\u0001\u0000\u0000\u0000\u0134\u0135\u0001"+
		"\u0000\u0000\u0000\u0135\u0136\u0005\u0013\u0000\u0000\u0136\u0155\u0001"+
		"\u0000\u0000\u0000\u0137\u013a\u0005\u0010\u0000\u0000\u0138\u0139\u0005"+
		"\'\u0000\u0000\u0139\u013b\u0003\u0006\u0003\u0000\u013a\u0138\u0001\u0000"+
		"\u0000\u0000\u013a\u013b\u0001\u0000\u0000\u0000\u013b\u013d\u0001\u0000"+
		"\u0000\u0000\u013c\u013e\u0003\f\u0006\u0000\u013d\u013c\u0001\u0000\u0000"+
		"\u0000\u013d\u013e\u0001\u0000\u0000\u0000\u013e\u013f\u0001\u0000\u0000"+
		"\u0000\u013f\u0155\u0005\u0011\u0000\u0000\u0140\u0141\u0005\u0010\u0000"+
		"\u0000\u0141\u0146\u0003\b\u0004\u0000\u0142\u0143\u0005\u0003\u0000\u0000"+
		"\u0143\u0145\u0003\b\u0004\u0000\u0144\u0142\u0001\u0000\u0000\u0000\u0145"+
		"\u0148\u0001\u0000\u0000\u0000\u0146\u0144\u0001\u0000\u0000\u0000\u0146"+
		"\u0147\u0001\u0000\u0000\u0000\u0147\u014d\u0001\u0000\u0000\u0000\u0148"+
		"\u0146\u0001\u0000\u0000\u0000\u0149\u014e\u0005\u0003\u0000\u0000\u014a"+
		"\u014b\u0005\u0003\u0000\u0000\u014b\u014c\u0005\'\u0000\u0000\u014c\u014e"+
		"\u0003\u0006\u0003\u0000\u014d\u0149\u0001\u0000\u0000\u0000\u014d\u014a"+
		"\u0001\u0000\u0000\u0000\u014d\u014e\u0001\u0000\u0000\u0000\u014e\u0150"+
		"\u0001\u0000\u0000\u0000\u014f\u0151\u0003\f\u0006\u0000\u0150\u014f\u0001"+
		"\u0000\u0000\u0000\u0150\u0151\u0001\u0000\u0000\u0000\u0151\u0152\u0001"+
		"\u0000\u0000\u0000\u0152\u0153\u0005\u0011\u0000\u0000\u0153\u0155\u0001"+
		"\u0000\u0000\u0000\u0154\u0119\u0001\u0000\u0000\u0000\u0154\u011a\u0001"+
		"\u0000\u0000\u0000\u0154\u0123\u0001\u0000\u0000\u0000\u0154\u0137\u0001"+
		"\u0000\u0000\u0000\u0154\u0140\u0001\u0000\u0000\u0000\u0155\u0007\u0001"+
		"\u0000\u0000\u0000\u0156\u0159\u0005>\u0000\u0000\u0157\u0158\u0005\u001d"+
		"\u0000\u0000\u0158\u015a\u0003\n\u0005\u0000\u0159\u0157\u0001\u0000\u0000"+
		"\u0000\u0159\u015a\u0001\u0000\u0000\u0000\u015a\t\u0001\u0000\u0000\u0000"+
		"\u015b\u015e\u0003\u0006\u0003\u0000\u015c\u015d\u0005\u0005\u0000\u0000"+
		"\u015d\u015f\u0003\u0004\u0002\u0000\u015e\u015c\u0001\u0000\u0000\u0000"+
		"\u015e\u015f\u0001\u0000\u0000\u0000\u015f\u000b\u0001\u0000\u0000\u0000"+
		"\u0160\u0161\u0005\u001e\u0000\u0000\u0161\u0162\u0005>\u0000\u0000\u0162"+
		"\r\u0001\u0000\u0000\u0000\u0163\u0164\u0005?\u0000\u0000\u0164\u000f"+
		"\u0001\u0000\u0000\u0000\u0165\u016a\u0003\u0012\t\u0000\u0166\u0167\u0005"+
		"\u0003\u0000\u0000\u0167\u0169\u0003\u0012\t\u0000\u0168\u0166\u0001\u0000"+
		"\u0000\u0000\u0169\u016c\u0001\u0000\u0000\u0000\u016a\u0168\u0001\u0000"+
		"\u0000\u0000\u016a\u016b\u0001\u0000\u0000\u0000\u016b\u016e\u0001\u0000"+
		"\u0000\u0000\u016c\u016a\u0001\u0000\u0000\u0000\u016d\u016f\u0005\u0003"+
		"\u0000\u0000\u016e\u016d\u0001\u0000\u0000\u0000\u016e\u016f\u0001\u0000"+
		"\u0000\u0000\u016f\u0011\u0001\u0000\u0000\u0000\u0170\u0171\u0005>\u0000"+
		"\u0000\u0171\u0172\u0005\u001d\u0000\u0000\u0172\u017d\u0003\u0004\u0002"+
		"\u0000\u0173\u0174\u0003\u000e\u0007\u0000\u0174\u0175\u0005\u001d\u0000"+
		"\u0000\u0175\u0176\u0003\u0004\u0002\u0000\u0176\u017d\u0001\u0000\u0000"+
		"\u0000\u0177\u0178\u0005\u0006\u0000\u0000\u0178\u0179\u0003\u0004\u0002"+
		"\u0000\u0179\u017a\u0005\u001f\u0000\u0000\u017a\u017b\u0003\u0004\u0002"+
		"\u0000\u017b\u017d\u0001\u0000\u0000\u0000\u017c\u0170\u0001\u0000\u0000"+
		"\u0000\u017c\u0173\u0001\u0000\u0000\u0000\u017c\u0177\u0001\u0000\u0000"+
		"\u0000\u017d\u0013\u0001\u0000\u0000\u0000*\u0017$2:>IZkn|\u008f\u0093"+
		"\u00af\u00b6\u00ba\u00bf\u00c3\u00c7\u00cd\u00d1\u00d4\u00f8\u0102\u0108"+
		"\u0114\u0116\u011d\u0120\u0129\u0130\u0133\u013a\u013d\u0146\u014d\u0150"+
		"\u0154\u0159\u015e\u016a\u016e\u017c";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}