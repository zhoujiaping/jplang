package jp.lang;

public class Util {
    public static void assertArgLen(Object[] args, int len) {
        if (args.length != len) {
            throw new RuntimeException("args count must be " + len + "!");
        }
    }
}
