package jp.lang;

import io.vavr.Tuple2;
import io.vavr.collection.Vector;
import jp.JpBaseVisitor;
import jp.JpLexer;
import jp.JpParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.commons.text.StringEscapeUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

import static jp.lang.BuiltIn.INVOKE_FN_NAME;

/*
 *
 */
public class ExecutionPhase extends JpBaseVisitor {
    private static final Object[] EMPTY_OBJ_ARR = {};

    private BuiltIn builtIn;
    private Frame curFrame;
    private Object result;

    private LinkedList destructStack = new LinkedList();

    private SemanticPhase semanticPhase;


    public ExecutionPhase() {
        curFrame = new Frame(null, null);
        builtIn = new BuiltIn();
        builtIn.allFn().put(INVOKE_FN_NAME, new Fn() {
            @Override
            public Object applyFn(Fn f, Object... args) {
                return invokeFn(f, args);
            }
        }.setDesc(INVOKE_FN_NAME));
        semanticPhase = new SemanticPhase(builtIn);
    }

    public Object getResult() {
        return result;
    }

    @Override
    public Object visitFile(JpParser.FileContext ctx) {
        result = RT.NIL;
        for (JpParser.StmtContext t : ctx.stmt()) {
            semanticPhase.visitStmt(t);
            result = visit(t);
        }
        return result;
    }

    @Override
    public Object visitStmt(JpParser.StmtContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Object visitAritBin(JpParser.AritBinContext ctx) {
        Object arg1 = (visit(ctx.expr(0)));
        Object arg2 = (visit(ctx.expr(1)));
        Fn f = OpFn.getBinFnByType(ctx.op.getType());
        return f.apply(arg1, arg2);
    }

    @Override
    public Object visitStr(JpParser.StrContext ctx) {
        String text = trimN(ctx.STRING().getText(), 1);
        return StringEscapeUtils.unescapeJava(text);
    }

    @Override
    public Object visitTmplStr(JpParser.TmplStrContext ctx) {
        //TODO 先不支持
        /*
         * 模板字符串
         * */
        String text = trimN(ctx.getText(), 3);//"""
        //TODO 字符串反转义，先用apache-commons-text顶一下
        return StringEscapeUtils.unescapeJava(text);
    }

    @Override
    public Object visitComp(JpParser.CompContext ctx) {
        Object arg1 = (visit(ctx.expr(0)));
        Object arg2 = (visit(ctx.expr(1)));
        Fn f = OpFn.getBinFnByType(ctx.op.getType());
        return f.apply(arg1, arg2);
    }

    @Override
    public Object visitZero(JpParser.ZeroContext ctx) {
        return 0L;
    }

    @Override
    public Object visitNull(JpParser.NullContext ctx) {
        return RT.NIL;
    }


    @Override
    public Object visitThrow(JpParser.ThrowContext ctx) {
        throw (RuntimeException) visit(ctx.expr());
    }


    @Override
    public Object visitSign(JpParser.SignContext ctx) {
        Object arg1 = (visit(ctx.expr(0)));
        Object arg2 = (visit(ctx.expr(1)));
        Fn f = OpFn.getBinFnByType(ctx.op.getType());
        return f.apply(arg1, arg2);
    }


    @Override
    public Object visitPre(JpParser.PreContext ctx) {
        Object arg1 = (visit(ctx.expr()));
        Fn f = OpFn.getPreFnByType(ctx.op.getType());
        return f.apply(arg1);
    }

    @Override
    public Object visitTry(JpParser.TryContext ctx) {
        //TODO 符号表生成，自由变量检查
        //创建栈帧
//        Object ret;
//        try{
//            ret = visit(ctx.expr(0));
//        }catch (Exception e){
//            destructStack.push(e);
//            visit(ctx.struct());
//            destructStack.pop();
//        }finally {
//
//        }
        return super.visitTry(ctx);
    }

    @Override
    public Object visitRange(JpParser.RangeContext ctx) {
        Object arg1 = (visit(ctx.expr(0)));
        Object arg2 = (visit(ctx.expr(1)));
        Fn f = OpFn.getBinFnByType(ctx.op.getType());
        return f.apply(arg1, arg2);
    }


    @Override
    public Object visitBitLogic(JpParser.BitLogicContext ctx) {
        Object arg1 = (visit(ctx.expr(0)));
        Object arg2 = (visit(ctx.expr(1)));
        Fn f = OpFn.getBinFnByType(ctx.op.getType());
        return f.apply(arg1, arg2);
    }

    @Override
    public Object visitLogic(JpParser.LogicContext ctx) {
        Object arg1 = (visit(ctx.expr(0)));
        Object arg2 = (visit(ctx.expr(1)));
        Fn f = OpFn.getBinFnByType(ctx.op.getType());
        return f.apply(arg1, arg2);
    }

    @Override
    public Object visitFnCall0(JpParser.FnCall0Context ctx) {
        Fn f = (Fn) visit(ctx.expr(0));
        Object[] params = EMPTY_OBJ_ARR;
        if (ctx.EXTEND() != null) {
            Vector vec = (Vector) visit(ctx.expr(1));
            params = vec.toJavaArray();
        }
        return invokeFn(f, params);
    }

    @Override
    public Object visitFnCallN(JpParser.FnCallNContext ctx) {
        Fn f = (Fn) visit(ctx.expr(0));
        Object[] params;
        if (ctx.EXTEND() != null) {
            List<JpParser.ExprContext> exprs = ctx.expr();
            List<Object> paramList = new ArrayList<>();
            for (int i = 1; i < exprs.size() - 1; i++) {
                paramList.add(visit(ctx.expr(i)));
            }
            List<Object> rest = (List<Object>) visit(ctx.expr(exprs.size() - 1));
            paramList.addAll(rest);
            params = paramList.toArray();
        } else {
            List<JpParser.ExprContext> exprs = ctx.expr();
            params = new Object[exprs.size() - 1];
            for (int i = 1; i < exprs.size(); i++) {
                params[i - 1] = visit(ctx.expr(i));
            }
        }
        return invokeFn(f, params);
    }

    private Object invokeFn(Fn f, Object[] params) {
        Object ret;
        while (true) {
            curFrame = new Frame(curFrame, f.freeVars);
            if (f.argCtxs != null) {  // 为null，则是手动new的Fn，没有ast，不需要解构。
                destructStack.push(Vector.of(params));
                Vector args = destructVec(f.argCtxs, f.extendCtx, f.asCtx);
                params = args.toJavaArray();
                destructStack.pop();
            }
            ret = f.apply(params);
            curFrame = curFrame.prev;
            if (ret instanceof Cont cont) {
                params = cont.args.toArray();
            } else {
                break;
            }
        }
        return ret;
    }

    @Override
    public Object visitBracket(JpParser.BracketContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Object visitCond(JpParser.CondContext ctx) {
        List<JpParser.ExprContext> exprs = ctx.expr();
        for (int i = 0; i < exprs.size() - 1; i = i + 2) {
            if (RT.truth(visit(exprs.get(i)))) {
                return visit(exprs.get(i + 1));
            }
        }
        return RT.NIL;
    }

    @Override
    public Object visitPow(JpParser.PowContext ctx) {
        Object arg1 = (visit(ctx.expr(0)));
        Object arg2 = (visit(ctx.expr(1)));
        Fn f = OpFn.getBinFnByType(JpLexer.POW);
        return f.apply(arg1, arg2);
    }

//    @Override
//    public Object visitByte(JpParser.ByteContext ctx) {
//        return super.visitByte(ctx);
//    }

    @Override
    public Object visitFalse(JpParser.FalseContext ctx) {
        return Boolean.FALSE;
    }

    @Override
    public Object visitEqual(JpParser.EqualContext ctx) {
        Object arg1 = (visit(ctx.expr(0)));
        Object arg2 = (visit(ctx.expr(1)));
        Fn f = OpFn.getBinFnByType(ctx.op.getType());
        return f.apply(arg1, arg2);
    }

    @Override
    public Object visitInt10(JpParser.Int10Context ctx) {
        String text = ctx.INT10().getText();
        return Maths.parseJavaLong(text);
    }

    @Override
    public Object visitInt16(JpParser.Int16Context ctx) {
        String text = ctx.INT16().getText();
        return Maths.parseJavaLong(text);
    }


    @Override
    public Object visitIntR(JpParser.IntRContext ctx) {
        String text = ctx.INT_R().getText();
        return Maths.parseJavaLong(text);
    }

    @Override
    public Object visitFloat(JpParser.FloatContext ctx) {
        String text = ctx.FLOAT().getText();
        return Maths.parseJavaDouble(text);
    }


    @Override
    public Object visitShift(JpParser.ShiftContext ctx) {
        Object arg1 = (visit(ctx.expr(0)));
        Object arg2 = (visit(ctx.expr(1)));
        Fn f = OpFn.getBinFnByType(ctx.op.getType());
        return f.apply(arg1, arg2);
    }

    @Override
    public Object visitChar(JpParser.CharContext ctx) {
        String text = trimN(ctx.getText(), 1);
        return StringEscapeUtils.unescapeJava(text).charAt(0);
    }

    @Override
    public Object visitTrue(JpParser.TrueContext ctx) {
        return Boolean.TRUE;
    }

    @Override
    public Object visitNameStruct(JpParser.NameStructContext ctx) {
        Object v = destructStack.peek();
        curFrame.setValue(ctx.NAME().getText(), v);
        return v;
    }

    @Override
    public Object visitNameRef(JpParser.NameRefContext ctx) {
        //当前栈帧中未找到，则找内置函数
        String name = ctx.NAME().getText();
        Ref ref = curFrame.findRef(name);
        if (ref == null)
            return builtIn.allFn().get(name);
        else
            return ref.value;
    }


    @Override
    public Object visitIf(JpParser.IfContext ctx) {
        Object predRes = visit(ctx.expr(0));
        if (RT.truth(predRes))
            return visit(ctx.expr(1));
        else if (ctx.expr().size() < 3) //no else
            return RT.NIL;
        else
            return visit(ctx.expr(2));
    }

    @Override
    public Object visitMap(JpParser.MapContext ctx) {
        JpParser.KvPairsContext kvCtx = ctx.kvPairs();
        List<JpParser.KvPairContext> pairCtxs = kvCtx.kvPair();
        io.vavr.collection.HashMap map = io.vavr.collection.HashMap.empty();
        for (JpParser.KvPairContext pairCtx : pairCtxs) {
            if (pairCtx instanceof JpParser.NamePairContext npCtx)
                map = map.put(npCtx.NAME().getText(), visit(npCtx.expr()));
            else if (pairCtx instanceof JpParser.StrPairContext spCtx)
                map = map.put(visitStr(spCtx.str()), visit(spCtx.expr()));
            else if (pairCtx instanceof JpParser.ExprPairContext epCtx)
                map = map.put(visit(epCtx.expr(0)), visit(epCtx.expr(1)));
        }
        return map;
    }

    private String trimN(String str, int n) {
        int len = str.length();
        return str.substring(n, len - n);
    }


    @Override
    public Object visitDecl(JpParser.DeclContext ctx) {
        ctx.NAME().forEach(it -> curFrame.setValue(it.getText(), null));
        return RT.NIL;
    }

    @Override
    public Object visitDef(JpParser.DefContext ctx) {
        Object ret = RT.NIL;
        int size = ctx.expr().size();
        for (int i = 0; i < size; i++) {
            destructStack.push(visit(ctx.expr(i)));
            ret = visit(ctx.struct(i));
            destructStack.pop();
        }
        return ret;
    }


    @Override
    public Object visitVecStruct0(JpParser.VecStruct0Context ctx) {
        return destructVec(Collections.emptyList(), ctx.struct(), ctx.as());
    }

    @Override
    public Object visitVecStructN(JpParser.VecStructNContext ctx) {
        return destructVec(ctx.structDefault(), ctx.struct(), ctx.as());
    }

    private Vector destructVec(List<JpParser.StructDefaultContext> itemCtxs,
                               JpParser.StructContext extendCtx,
                               JpParser.AsContext asCtx) {
        Vector vec = (Vector) destructStack.peek();
        /*
         * def vec = Vector.of(1,2,3)
         * println(vec.splitAt(1))
         * => (Vector(1), Vector(2, 3))
         * println(vec.splitAt(5))
         * => (Vector(1, 2, 3), Vector())
         *  */
        if (vec == RT.NIL) {
            vec = Vector.empty();
        }
        Tuple2<Vector, Vector> tuple2 = vec.splitAt(itemCtxs.size());
        Vector vec1 = tuple2._1;
        Vector newVec = Vector.empty();
        for (int i = 0; i < itemCtxs.size(); i++) {
            JpParser.StructDefaultContext itemCtx = itemCtxs.get(i);
            Object item = RT.NIL;
            if (i < vec.length()) {
                item = vec1.get(i);
            }
            destructStack.push(item);
            newVec = newVec.append(visit(itemCtx));
            destructStack.pop();
        }
        if (extendCtx != null) {
            destructStack.push(tuple2._2);//rest
            newVec = newVec.appendAll((Vector) visit(extendCtx));
            destructStack.pop();
        }
        if (asCtx != null) {
            curFrame.setValue(asCtx.NAME().getText(), vec);
        }
        return newVec;
    }

    @Override
    public Object visitMapStruct0(JpParser.MapStruct0Context ctx) {
        return destructMap(Collections.emptyList(), ctx.struct(), ctx.as());
    }

    @Override
    public Object visitMapStructN(JpParser.MapStructNContext ctx) {
        return destructMap(ctx.structPair(), ctx.struct(), ctx.as());
    }

    private io.vavr.collection.Map destructMap(List<JpParser.StructPairContext> pairCtxs,
                                               JpParser.StructContext extendCtx,
                                               JpParser.AsContext asCtx) {
        //被解构的map
        io.vavr.collection.Map map = (io.vavr.collection.Map) destructStack.peek();
        //已经被收集的keys
        io.vavr.collection.Set collectedKeys = io.vavr.collection.HashSet.empty();
        //被收集的map
        io.vavr.collection.Map newMap = io.vavr.collection.HashMap.empty();

        for (JpParser.StructPairContext pairCtx : pairCtxs) {
            String name = pairCtx.NAME().getText();
            collectedKeys.add(name);
            Object value = map.getOrElse(name, RT.NIL);

            if (pairCtx.structDefault() == null) {
                curFrame.setValue(name, value);
            } else {
                destructStack.push(value);
                value = visit(pairCtx.structDefault());
                destructStack.pop();
            }
            newMap = newMap.put(name, value);
        }
        if (extendCtx != null) {
            //剩余pairs的map
            destructStack.push(map.removeAll(collectedKeys));
            newMap = newMap.merge((io.vavr.collection.Map) visit(extendCtx));
            destructStack.pop();
        }
        if (asCtx != null) {
            curFrame.setValue(asCtx.NAME().getText(), newMap);
        }
        return newMap;
    }

    @Override
    public Object visitStructDefault(JpParser.StructDefaultContext ctx) {
        Object v = visit(ctx.struct());
        if (ctx.expr() != null && v == null) {
            v = visit(ctx.expr());
        }
        return v;
    }

    @Override
    public Object visitStructPair(JpParser.StructPairContext ctx) {
        return super.visitStructPair(ctx);
    }


    @Override
    public Object visitFn0(JpParser.Fn0Context ctx) {
        return defn(ctx, Collections.emptyList(), ctx.struct(), ctx.str(), ctx.expr());
    }


    private Object visitStmts(List<JpParser.StmtContext> stmts) {
        Object r = RT.NIL;
        for (JpParser.StmtContext stmt : stmts) {
            r = visit(stmt);
        }
        return r;
    }

    @Override
    public Object visitFnN(JpParser.FnNContext ctx) {
        return defn(ctx, ctx.structDefault(), ctx.struct(), ctx.str(), ctx.expr());
    }

    private Fn defn(JpParser.ExprContext ctx, List<JpParser.StructDefaultContext> defaultCtxs,
                    JpParser.StructContext extCtx,
                    JpParser.StrContext descCtx,
                    JpParser.ExprContext bodyExprCtx) {
        Fn f = new Fn() {
            @Override
            public Object apply(Object... args) {
                for (int i = 0; i < defaultCtxs.size(); i++) {
                    Object arg = RT.NIL;
                    if (i < args.length) {
                        arg = args[i];
                    }
                    destructStack.push(arg);
                    visit(defaultCtxs.get(i));
                    destructStack.pop();
                }
                return visit(bodyExprCtx);
            }
        };
        SymTable t = semanticPhase.symsMap.get(ctx);
        f.freeVars = curFrame.findRefs(t.freeSyms.keySet());
        f.argCtxs = defaultCtxs;
        f.extendCtx = extCtx;
        if (descCtx != null) {
            f.desc = (String) visit(descCtx);
        }
        return f;
    }


    @Override
    public Object visitLoop(JpParser.LoopContext ctx) {
        List args = new ArrayList();
        for (int i = 0; i < ctx.structDefault().size(); i++) {
            args.add(visit(ctx.expr(i)));
        }
        while (true) {
            List finalArgs = args;
            Object r = withNewFrame(ctx, () -> {
                for (int i = 0; i < finalArgs.size(); i++) {
                    destructStack.push(finalArgs.get(i));
                    visitStructDefault(ctx.structDefault(i));
                    destructStack.pop();
                }
                return visit(ctx.expr(ctx.expr().size() - 1));
            });
            if (r instanceof Cont cont) {
                args = cont.args;
            } else {
                return r;
            }
        }
    }

    private Object withNewFrame(ParserRuleContext ctx, Supplier f) {
        SymTable t = semanticPhase.symsMap.get(ctx);
        curFrame = new Frame(curFrame, curFrame.findRefs(t.freeSyms.keySet()));
        Object r = f.get();
        curFrame = curFrame.prev;
        return r;
    }


    @Override
    public Object visitRecur0(JpParser.Recur0Context ctx) {
        return Cont.EMPTY;
    }

    @Override
    public Object visitRecurN(JpParser.RecurNContext ctx) {
        List<JpParser.ExprContext> exprs = ctx.expr();
        List args = new ArrayList();
        for (JpParser.ExprContext expr : exprs) {
            args.add(visit(expr));
        }
        return new Cont(args);
    }

    @Override
    public Object visitBlock(JpParser.BlockContext ctx) {
        return withNewFrame(ctx, () -> {
            Object r = RT.NIL;
            for (JpParser.StmtContext stmt : ctx.stmt()) {
                r = visit(stmt.expr());
            }
            return r;
        });
    }

    @Override
    public Object visitEmptyVec(JpParser.EmptyVecContext ctx) {
        return Vector.empty();
    }

    @Override
    public Object visitVec(JpParser.VecContext ctx) {
        Vector v = Vector.empty();
        List<JpParser.ExprContext> exprs = ctx.expr();
        for (JpParser.ExprContext e : exprs) {
            v = v.append(visit(e));
        }
        return v;
    }


    @Override
    public Object visitVisitAttr(JpParser.VisitAttrContext ctx) {
        io.vavr.collection.Map m = (io.vavr.collection.Map) visit(ctx.expr());
        return m.get(ctx.NAME().getText());
    }

    @Override
    public Object visitVisitExprAttr(JpParser.VisitExprAttrContext ctx) {
        io.vavr.collection.Map m = (io.vavr.collection.Map) visit(ctx.expr(0));
        return m.get(visit(ctx.expr(1)));
    }

    ThreadLocal holder = new ThreadLocal();

    @Override
    public Object visitPip(JpParser.PipContext ctx) {
        //1 |> _ +1 |> println(_)  ;
        /*
         * do(
         *   def _ = 1;
         *   def _ = _ +1;
         *   def _ = println(_);
         * );
         * */
        Object oldV = holder.get();
        try {
            holder.set(visit(ctx.expr(0)));
            return visit(ctx.expr(1));
        } finally {
            holder.set(oldV);
        }
    }

    @Override
    public Object visitUnderline(JpParser.UnderlineContext ctx) {
        return holder.get();
    }
}
