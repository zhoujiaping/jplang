package jp.lang;

import jp.JpLexer;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public abstract class OpFn extends Fn {
    private static final Map<String, OpFn> nameToPreFns = new HashMap<>();
    private static final Map<String, OpFn> nameToBinFns = new HashMap<>();
    private static final Map<String, OpFn> allFns = new HashMap<>();

    private static final Map<Integer, OpFn> typeToPreFns = new HashMap<>();
    private static final Map<Integer, OpFn> typeToBinFns = new HashMap<>();
    private int type;
    private String name;

    protected OpFn(int type, String name) {
        this.type = type;
        this.name = name;
        this.desc = name;
    }

    static {
        OpFn eqFn = new OpFn(JpLexer.EQ, "eq") {
            @Override
            public Object apply(Object... args) {
                boolean firstIsNumber = args[0] instanceof Number;
                for (int i = 1; i < args.length; i++) {
                    if (firstIsNumber && args[i] instanceof Number) {
                        if (!Maths.eq((Number) args[0], (Number) args[i])) {
                            return false;
                        }
                    } else {
                        if (!Objects.equals(args[0], args[i])) {
                            return false;
                        }
                    }
                }
                return true;
            }
        };
        addPreFns(new OpFn(JpLexer.NOT, "not") {
            @Override
            public Object apply(Object... args) {
                return !RT.truth(args[0]);
            }
        }, new OpFn(JpLexer.BIT_NEG, "bitNeg") {
            @Override
            public Object apply(Object... args) {
                // ~x=-x-1
                return Maths.bitNeg((Number) args[0]);
            }
        }, new OpFn(JpLexer.ADD, "pos") {
            @Override
            public Object apply(Object... args) {
                return Maths.pos((Number) args[0]);
            }
        }, new OpFn(JpLexer.SUB, "neg") {
            @Override
            public Object apply(Object... args) {
                return Maths.neg((Number) args[0]);
            }
        });

        addBinFn(new OpFn(JpLexer.POW, "pow") {
            @Override
            public Object apply(Object... args) {
                return Maths.pow((Number) args[0], (Number) args[1]);
            }
        }, new OpFn(JpLexer.MUL, "mul") {
            @Override
            public Object apply(Object... args) {
                Object r = args[0];
                for (int i = 1; i < args.length; i++) {
                    r = Maths.mul((Number) r, (Number) args[i]);
                }
                return r;
            }
        }, new OpFn(JpLexer.DIV, "div") {
            @Override
            public Object apply(Object... args) {
                return Maths.div((Number) args[0], (Number) args[1]);
            }
        }, new OpFn(JpLexer.REM, "rem") {
            @Override
            public Object apply(Object... args) {
                return Maths.rem((Number) args[0], (Number) args[1]);
            }
        }, new OpFn(JpLexer.ADD, "add") {
            @Override
            public Object apply(Object... args) {
                //可能溢出，会自动升级类型，或者后面的参数是更高级的类型。所以需要重新获取ops
                Object r = args[0];
                for (int i = 1; i < args.length; i++) {
                    r = Maths.add((Number) r, (Number) args[i]);
                }
                return r;
            }
        }, new OpFn(JpLexer.SUB, "sub") {
            @Override
            public Object apply(Object... args) {
                Object r = args[0];
                for (int i = 1; i < args.length; i++) {
                    r = Maths.sub((Number) r, (Number) args[i]);
                }
                return r;
            }
        }, new OpFn(JpLexer.SHIFT_LEFT, "shiftLeft") {
            @Override
            public Object apply(Object... args) {
                //左移位数必须是整数。（如果是大整数，浮点数，大浮点数，分数，必须能精确转换为整数）
                return Maths.shiftLeft((Number) args[0], (Number) args[1]);
            }
        }, new OpFn(JpLexer.SHIFT_RIGHT, "shiftRight") {
            @Override
            public Object apply(Object... args) {
                //右移位数必须是整数。（如果是大整数，浮点数，大浮点数，分数，必须能精确转换为整数）
                return Maths.shiftRight((Number) args[0], (Number) args[1]);
            }
        }, new OpFn(JpLexer.SHIFT_RIGHT_UNSIGNED, "shiftRightUnsigned") {
            @Override
            public Object apply(Object... args) {
                return Maths.shiftRightUnsigned((Number) args[0], (Number) args[1]);
            }
        }, new OpFn(JpLexer.LT, "lt") {
            @Override
            public Object apply(Object... args) {
                return Maths.lt((Number) args[0], (Number) args[1]);
            }
        }, new OpFn(JpLexer.LT_EQ, "ltEq") {
            @Override
            public Object apply(Object... args) {
                return Maths.ltEq((Number) args[0], (Number) args[1]);
            }
        }, new OpFn(JpLexer.GT, "gt") {
            @Override
            public Object apply(Object... args) {
                return Maths.gt((Number) args[0], (Number) args[1]);
            }
        }, new OpFn(JpLexer.GT_EQ, "gtEq") {
            @Override
            public Object apply(Object... args) {
                return Maths.gtEq((Number) args[0], (Number) args[1]);
            }
        }, new OpFn(JpLexer.EQ, "eq") {
            @Override
            public Object apply(Object... args) {
                return eqFn.apply(args);
            }
        }, new OpFn(JpLexer.NOT_EQ, "notEq") {
            @Override
            public Object apply(Object... args) {
                return !(Boolean) eqFn.apply(args);
            }
        }, new OpFn(JpLexer.BIT_AND, "bitAnd") {
            @Override
            public Object apply(Object... args) {
                Object r = args[0];
                for (int i = 1; i < args.length; i++) {
                    r = Maths.bitAnd((Number) r, (Number) args[i]);
                }
                return r;
            }
        }, new OpFn(JpLexer.BIT_XOR, "bitXor") {
            @Override
            public Object apply(Object... args) {
                Object r = args[0];
                for (int i = 1; i < args.length; i++) {
                    r = Maths.bitXor((Number) r, (Number) args[i]);
                }
                return r;
            }
        }, new OpFn(JpLexer.BIT_OR, "bitOr") {
            @Override
            public Object apply(Object... args) {
                Object r = args[0];
                for (int i = 1; i < args.length; i++) {
                    r = Maths.bitOr((Number) r, (Number) args[i]);
                }
                return r;
            }
        }, new OpFn(JpLexer.AND, "and") {
            @Override
            public Object apply(Object... args) {
                boolean r = RT.truth(args[0]);
                for (int i = 1; i < args.length; i++) {
                    if (!r) return false;
                    r = r && RT.truth(args[i]);
                }
                return r;
            }
        }, new OpFn(JpLexer.OR, "or") {
            @Override
            public Object apply(Object... args) {
                boolean r = RT.truth(args[0]);
                for (int i = 1; i < args.length; i++) {
                    if (r) return true;
                    r = r || RT.truth(args[i]);
                }
                return r;
            }
        }, new OpFn(JpLexer.RANGE, "range") {
            @Override
            public Object apply(Object... args) {
                if (args[0] instanceof Character) {
                    throw new RuntimeException("not implement yet");//TODO
                } else if (args[0] instanceof Number) {
                    if (args.length == 2)
                        return Maths.range((Number) args[0], (Number) args[1]);
                    else
                        return Maths.range((Number) args[0], (Number) args[1], (Number) args[2]);
                } else {
                    throw new RuntimeException("expect Number/Char, but is " + args[0].getClass());
                }
            }
        }, new OpFn(JpLexer.RANGE_TO, "rangeTo") {
            @Override
            public Object apply(Object... args) {
                if (args[0] instanceof Character) {
                    throw new RuntimeException("not implement yet");//TODO
                } else if (args[0] instanceof Number) {
                    if (args.length == 2)
                        return Maths.rangeTo((Number) args[0], (Number) args[1]);
                    else
                        return Maths.rangeTo((Number) args[0], (Number) args[1], (Number) args[2]);
                } else {
                    throw new RuntimeException("expect Number/Char, but is " + args[0].getClass());
                }
            }
        });
    }

    private static void addPreFns(OpFn... fns) {
        for (OpFn fn : fns) {
            nameToPreFns.put(fn.name, fn);
            typeToPreFns.put(fn.type,fn);
            allFns.put(fn.name,fn);
        }
    }

    private static void addBinFn(OpFn... fns) {
        for (OpFn fn : fns) {
            nameToBinFns.put(fn.name, fn);
            typeToBinFns.put(fn.type,fn);
            allFns.put(fn.name,fn);
        }
    }
    public static OpFn getPreFnByType(int type){
        return typeToPreFns.get(type);
    }

    public static OpFn getBinFnByType(int type){
        return typeToBinFns.get(type);
    }

    public static Map<String,OpFn> allFn(){
        return allFns;
    }
}
