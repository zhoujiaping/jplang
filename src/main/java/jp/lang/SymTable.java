package jp.lang;

import java.util.HashMap;

public class SymTable {
    protected SymTable parent;
    protected java.util.Map<String, Object> syms = new HashMap<>();
    protected java.util.Map<String, Object> freeSyms = new HashMap<>();
    protected java.util.Map<String, Object> declSyms = new HashMap<>();

    protected SymTable(SymTable p) {
        this.parent = p;
    }

    protected void onRefSym(BuiltIn builtIn, String name) {
        //引用符号时，如果已经在符号表，则ok。
        if (syms.containsKey(name)) {
            return;
        }
        //如果已经在declare的符号表中，说明被declare了，但是没有def，则不能引用。
        if (declSyms.containsKey(name)) {
            throw new RuntimeException(name + "has not been init");
        }
        //符号表和declare的符号表中都没有，则是自由变量。
        freeSyms.put(name, null);

        //对于自由变量，要对符号表构成的树，从当前符号表向上查找，知道找到定义了这个自由变量。
        //中间所经过的符号表，都必须持有声明引用了该自由变量。
        SymTable t = this.parent;
        while (t != null) {
            if (t.syms.containsKey(name) || t.declSyms.containsKey(name)) {
                break;
            } else {
                t.freeSyms.put(name, null);
                t = t.parent;
            }
        }
        //如果都找不到，则查找内置符号表。如果内置符号表也没有，说明符号未定义，抛异常。
        if (t == null && !builtIn.allFn().containsKey(name)) {
            throw new RuntimeException("undefined " + name);
        }
    }

    protected void addSym(String name) {
        //已经被认为是自由变量，则不能再declare/def。
        // def f = fn() do(
        //     println(x);//x为自由变量
        //     def x = 1;//然后又定义x。这种操作是非法的。
        // );
        if (freeSyms.containsKey(name)) {
            throw new RuntimeException(name + "already in free syms");
        }
        declSyms.remove(name);
        syms.put(name, null);
    }

    protected void addSymDecl(String name) {
        if (freeSyms.containsKey(name)) {
            throw new RuntimeException(name + "already in free syms");
        }
        //允许重新declare，允许已经def的变量再declare
        syms.remove(name);
        declSyms.put(name, null);
    }

}
