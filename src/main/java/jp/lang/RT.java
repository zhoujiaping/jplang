package jp.lang;

import io.vavr.collection.HashMap;
import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.Map;
import jp.JpLexer;
import jp.JpParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CodePointCharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.DiagnosticErrorListener;
import org.antlr.v4.runtime.atn.PredictionMode;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Scanner;

public class RT {

    public static final Object NIL = null;
    /**
     * 模块查找路径，逗号分隔。
     */
    public static String modpath;
    //modName->Mod
    private static Map<String, Mod> modCache = HashMap.empty();
    /**
     * 正在执行的模块处于最后一个节点
     */
    private static LinkedHashMap<String, Mod> loadingMods = LinkedHashMap.empty();

    public static boolean truth(Object b) {
        if (b == RT.NIL) return false;
        if (b instanceof Boolean b1) return b1;
        return true;
    }


    /**
     * TODO 模块唯一标识？  如果使用路径，同一个路径有多种表示方法，会导致模块重复加载。
     * 可以对路径进行标准化，不同表示方法转换为同一种表示方法。
     * 但是这样就限定了模块代码只能放文件中。
     * 更好的做法是提供一个module关键字，一个export关键字。 module关键字后面跟模块名，模块名作为唯一id。
     */
    public static Mod require(String name) {
        if (loadingMods.containsKey(name)) {
            throw new RuntimeException("Cyclic loading module(" + name + ")");
        }
        Mod mod;
        try {
            loadingMods = loadingMods.put(name, null);
            mod = modCache.get(name).getOrElse(() -> requireForce(name));
        } finally {
            loadingMods = loadingMods.remove(name);
        }
        return mod;
    }

    private static File findModuleFile(String name) {
        String modpath = RT.modpath;
        String[] paths = modpath.trim().split("\s*,\s*");
        for (String path : paths) {
            File file = Paths.get(path, name + ".jp").toFile();
            if (!file.exists()) {
                continue;
            }
            return file;
        }
        throw new RuntimeException("module(" + name + ") is not found");
    }

    public static Mod requireForce(String name) {
        File file = findModuleFile(name);
        Mod mod = new Mod(file, name);
        try (InputStream modStream = new BufferedInputStream(new FileInputStream(file));) {
            mod.modObj = RT.eval(modStream);
            modCache = modCache.put(name, mod);
            return mod;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Object eval(File file) {
        try {
            return eval(new FileInputStream(file));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Object eval(InputStream modStream) {
        String code = readString(buffered(modStream));
        return eval(code);
    }

    protected static Object repl(java.util.Map<String, Object> opts){
        try {
            ExecutionPhase executionPhase = new ExecutionPhase();
            Scanner scanner = new Scanner(System.in);
            Exception ex;

            while (true) {
                try {
                    System.out.print("jp>");
                    String code = readString(scanner);
                    Object r = eval(executionPhase, code);
                    System.out.println(r);
                } catch (Exception e) {
                    ex = e;
                    System.err.println(e);
                }
            }
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    private static String readString(Scanner scanner) {
        StringBuilder sb = new StringBuilder();
        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
            sb.append(line).append('\n');
            if(line.endsWith("///")){
                return sb.toString();
            }
        }
        return null;
    }

    public static Object eval(String code) {
        return eval(new ExecutionPhase(), code);
    }

    public static Object eval(ExecutionPhase executionPhase, String code) {
        CodePointCharStream charStream = CharStreams.fromString(code);
        JpLexer lexer = new JpLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        JpParser parser = new JpParser(tokens);
        //报告所有歧义
        parser.getInterpreter().setPredictionMode(PredictionMode.LL_EXACT_AMBIG_DETECTION);
        parser.removeErrorListeners();
        parser.addErrorListener(new DiagnosticErrorListener());
        parser.setErrorHandler(new BailErrorStrategy());

        JpParser.FileContext tree = parser.file();

        executionPhase.visit(tree);
        return executionPhase.getResult();
    }

    private static BufferedInputStream buffered(InputStream input) {
        if (input instanceof BufferedInputStream it)
            return it;
        return new BufferedInputStream(input);
    }

    private static String readString(InputStream input) {
        try {
            InputStreamReader reader = new InputStreamReader(input, StandardCharsets.UTF_8);
            char[] buf = new char[1024 * 4];
            StringBuilder sb = new StringBuilder();
            int len = reader.read(buf);
            sb.append(buf, 0, len);
            return sb.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
