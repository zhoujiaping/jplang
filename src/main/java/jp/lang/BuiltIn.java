package jp.lang;

import io.vavr.collection.Stream;
import io.vavr.collection.Vector;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

public class BuiltIn {
    public static final String INVOKE_FN_NAME = "invoke";
    private final SecureRandom random = new SecureRandom();
    private Map<String,Fn> fns = new HashMap<>();

    {
        fns.put(INVOKE_FN_NAME,null);//add it later
        fns.putAll(OpFn.allFn());
        fns.put("rand", new Fn() {
            @Override
            public Object apply(Object... args) {
                if (args == null || args.length == 0)
                    return random.nextLong();
                else if (args.length > 1)
                    throw new RuntimeException("too many args for rand");
                else if (args[0] instanceof Long)
                    return random.nextLong((Long) args[0]);
                else if (args[0] instanceof Double) {
                    return random.nextDouble((Long) args[0]);
                } else {
                    throw new RuntimeException("unmatched arg type, require Long/Double, but is " + args[0].getClass());
                }
            }
        }.setDesc("rand"));
        fns.put("require", new Fn() {
            @Override
            public Object apply(Object... args) {
                return RT.require(args[0].toString());
            }
        }.setDesc("require"));
       
        fns.put("hashcode", new Fn() {
            @Override
            public Object apply(Object... args) {
                if(args[0] == null)
                    return 0;
                else if(args[0] instanceof Number it)
                    return Maths.hash(it);
                return args[0].hashCode();
            }
        }.setDesc("hashcode"));

        fns.put("println", new Fn() {
            @Override
            public Object apply(Object... args) {
                if (args == null || args.length == 0) {
                    System.out.println();
                    return RT.NIL;
                } else if (args.length > 1)
                    throw new RuntimeException("too many args for println");
                else {
                    System.out.println(args[0]);
                    return args[0];
                }
            }
        }.setDesc("println"));

        fns.put("exit", new Fn() {
            @Override
            public Object apply(Object... args) {
                int status = 0;
                if(args.length>0){
                    status = ((Number)args[0]).intValue();
                }
                System.exit(status);
                return null;
            }
        });

        fns.put("map", new Fn() {
            @Override
            public Object apply(Object... args) {
                Object coll = args[0];
                Fn f = (Fn) args[1];
                if(coll instanceof String s)
                    return Vector.ofAll(Stream.ofAll(s.toCharArray()).map(it-> fns.get(INVOKE_FN_NAME).applyFn(f,it)));
                if(coll instanceof Iterable<?> iter)
                    return Vector.ofAll(Stream.ofAll(iter).map(it->fns.get(INVOKE_FN_NAME).applyFn(f,it)));
                throw new IllegalArgumentException();
            }
        }.setDesc("map"));

        fns.put("filter", new Fn() {
            @Override
            public Object apply(Object... args) {
                Object coll = args[0];
                Fn f = (Fn) args[1];
                if(coll instanceof String s)
                    return Vector.ofAll(Stream.ofAll(s.toCharArray()).filter(it-> (Boolean)fns.get(INVOKE_FN_NAME).applyFn(f,it)));
                if(coll instanceof Iterable<?> iter)
                    return Vector.ofAll(Stream.ofAll(iter).filter(it->(Boolean)fns.get(INVOKE_FN_NAME).applyFn(f,it)));
                throw new IllegalArgumentException();
            }
        }.setDesc("filter"));
    }

    public Map<String,Fn> allFn(){
        return fns;
    }
}
