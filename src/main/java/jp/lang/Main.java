package jp.lang;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {

        Map<String,Object> options = parseArgs(args);
        if(options.get("file")==null){
            if(args.length==0){
                RT.repl(options);
                return;
            }
        }
        if(options.get("modpath") == null){
            throw new IllegalArgumentException("-modpath is not found!");
        }
        RT.modpath = options.get("modpath").toString();
        String filename = options.get("file").toString();
        RT.eval(new File(filename));
    }

    private static Map<String, Object> parseArgs(String[] args) {
        Map<String, Object> options = new HashMap<>();
        for(int i=0;i<args.length;i++){
            if(args[i].startsWith("--")){
                options.put(args[i].substring(2),null);
            }else if(args[i].startsWith("-")){
                options.put(args[i].substring(1),args[i+1]);
                i++;
            }else
                throw new IllegalArgumentException("format: ((-NAME VALUE)|--NAME)* ,eg: -user jack --nohup");
        }
        return options;
    }
}
