package jp.lang;

import jp.JpBaseVisitor;
import jp.JpParser;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.function.Supplier;

public class SemanticPhase extends JpBaseVisitor {
    protected ParseTreeProperty<SymTable> symsMap = new ParseTreeProperty<>();
    protected SymTable rootSyms;
    protected SymTable curSyms;

    protected BuiltIn builtIn;

    public SemanticPhase(BuiltIn builtIn){
        curSyms = rootSyms = new SymTable(null);
        this.builtIn = builtIn;
    }

    @Override
    public Object visitFile(JpParser.FileContext ctx) {
        return super.visitFile(ctx);
    }

    @Override
    public Object visitDecl(JpParser.DeclContext ctx) {
        for(TerminalNode it: ctx.NAME()){
            String name = it.getText();
            curSyms.addSymDecl(name);
        }
        return null;
    }

    @Override
    public Object visitDef(JpParser.DefContext ctx) {
        int size = ctx.expr().size();
        //def a = 1+1, b=2;
        for (int i = 0; i < size; i++) {
            //先对右侧表达式进行求值。
            visit(ctx.expr(i));
            //再解构左侧
            visit(ctx.struct(i));
        }
        return null;
    }

    /**
     * 对变量名称的引用，如果变量名称没有被声明，则标记为自由变量
     */
    @Override
    public Object visitNameRef(JpParser.NameRefContext ctx) {
        String name = ctx.NAME().getText();
        curSyms.onRefSym(builtIn, name);
        return null;
    }

    @Override
    public Object visitStructPair(JpParser.StructPairContext ctx) {
        String name = ctx.NAME().getText();
        curSyms.addSym(name);
        if(ctx.structDefault()!=null){
            return visit(ctx.structDefault());
        }
        return null;
    }

    @Override
    public Object visitNameStruct(JpParser.NameStructContext ctx) {
        curSyms.addSym(ctx.NAME().getText());
        return null;
    }


    @Override
    public Object visitAs(JpParser.AsContext ctx) {
        String name = ctx.NAME().getText();
        curSyms.addSym(name);
        return null;
    }

    @Override
    public Object visitFn0(JpParser.Fn0Context ctx) {
        return withNewSymTable(ctx,()-> super.visitFn0(ctx));
    }

    @Override
    public Object visitFnN(JpParser.FnNContext ctx) {
        return withNewSymTable(ctx,()-> super.visitFnN(ctx));
    }


    @Override
    public Object visitLoop(JpParser.LoopContext ctx) {
        return withNewSymTable(ctx,()->super.visitLoop(ctx));
    }


    @Override
    public Object visitBlock(JpParser.BlockContext ctx) {
        return withNewSymTable(ctx,()->super.visitBlock(ctx));
    }

    private Object withNewSymTable(JpParser.ExprContext ctx, Supplier f){
        curSyms = new SymTable(curSyms);
        symsMap.put(ctx,curSyms);
        Object r = f.get();
        curSyms = curSyms.parent;
        return r;
    }
}
