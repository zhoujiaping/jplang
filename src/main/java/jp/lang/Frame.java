package jp.lang;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Frame {
    Map<String, Ref> refs = new HashMap<>();

    public Frame prev;

    public Frame(Frame prev, Map<String, Ref> freeVars) {
        this.prev = prev;
        if (freeVars != null) {
            refs.putAll(freeVars);
        }
    }

    public Object findValue(String name) {
        if(refs.get(name)==null)
            throw new RuntimeException("can't find value for "+name);
        return refs.get(name).value;
    }

    public Ref findRef(String name) {
        return refs.get(name);
    }

    public void setValue(String name, Object value) {
        Ref ref = refs.get(name);
        if (ref == null) {
            ref = new Ref(value);
            refs.put(name, ref);
        } else {
            ref.value = value;
        }
    }

    public void setRef(String name, Ref ref) {
        refs.put(name, ref);
    }

    public Map<String, Ref> findRefs(Collection<String> freeSyms) {
        Map<String, Ref> refs = new HashMap<>();
        for (String name : freeSyms) {
            refs.put(name, this.refs.get(name));
        }
        return refs;
    }
}
