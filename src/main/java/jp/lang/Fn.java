package jp.lang;

import jp.JpParser;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public abstract class Fn {
    public java.util.Map<String,Ref> freeVars = new HashMap<>();

    public String desc;
    public List<JpParser.StructDefaultContext> argCtxs;
    public JpParser.StructContext extendCtx;
    public JpParser.AsContext asCtx;

    public Object apply(Object... args){
        throw new RuntimeException("not implement!");
    }
    public Object applyFn(Fn f, Object... args){
        throw new RuntimeException("not implement!");
    }

    public <T extends Fn> T setDesc(String desc){
        this.desc = desc;
        return (T) this;
    }
}
